# Chattie

A Messenger Application with most of the features like in Whatsapp. You can Register in the app and connect with your friends from your Contacts. You can chat with them and share photos, stickers etc. And Video and Audio Calling feature is also available. You can video chat with your friend.

## App UI

<img src="AppPreview/1.jpg" width="300"></br>

<img src="AppPreview/2.jpg" width="300"></br>

<img src="AppPreview/3.jpg" width="300"></br>

<img src="AppPreview/4.jpg" width="300"></br>

<img src="AppPreview/5.jpg" width="300"></br>

<img src="AppPreview/6.jpg" width="300"></br>

<img src="AppPreview/7.jpg" width="300"></br>
