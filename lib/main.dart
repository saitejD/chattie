import 'package:chattie/bloc/user_provider.dart';
import 'package:chattie/services/constants.dart';
import 'package:chattie/screens/splash_screen.dart';
import 'package:chattie/services/push_notification_service.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

// function to handle push notification on background
// Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
//   await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
//     alert: true,
//     badge: true,
//     sound: true,
//   );

//   LocalNotificationService notificationService = LocalNotificationService();
//   notificationService.showAwesomeNotification(message);
// }

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.transparent));

  PushNotificationService service = PushNotificationService();
  service.initialFirebaseMessaging();
  FirebaseMessaging.onBackgroundMessage(
      service.firebaseMessagingBackgroundHandler);

  String? res = await service.getFirebaseToken();
  print("res $res");

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<UserProvider>(
      create: (context) => UserProvider(),
      child: MaterialApp(
        title: 'Chattie',
        debugShowCheckedModeBanner: false,
        // theme: ThemeData(primaryColor: Colors.green),
        theme: ThemeData.light().copyWith(
          primaryColor: color1,
          appBarTheme: AppBarTheme(backgroundColor: color1),
          floatingActionButtonTheme:
              FloatingActionButtonThemeData(backgroundColor: color1),
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              backgroundColor: color1,
              minimumSize: Size(double.infinity, 56),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          // colorScheme:
          //     ColorScheme.fromSwatch().copyWith(secondary: Colors.blueGrey),
        ),
        home: SplashScreen(),
        // home: AgoraHomeScreen(),
      ),
    );
  }
}


/*
3. push notification on message
4. show caller dialog on app dismissed also

1. audio call from agora
2. wrap every screen with pickup layout
5. invite contacts in contacts screen
*/
