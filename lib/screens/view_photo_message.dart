import 'package:flutter/material.dart';
import 'package:extended_image/extended_image.dart';
import 'package:intl/intl.dart';

class ViewPhotoMessage extends StatefulWidget {
  final String url, friendName, sendTime;

  ViewPhotoMessage(
      {required this.url, required this.friendName, required this.sendTime});
  @override
  _ViewPhotoMessageState createState() => _ViewPhotoMessageState();
}

class _ViewPhotoMessageState extends State<ViewPhotoMessage> {
  String getDate(String d) {
    String date = '';
    if (int.parse(DateFormat('dd')
            .format(DateTime.fromMillisecondsSinceEpoch(int.parse(d)))) ==
        (int.parse(DateFormat("dd").format(DateTime.now())) - 1)) {
      String extra = DateFormat('hh:mm a')
          .format(DateTime.fromMillisecondsSinceEpoch(int.parse(d)));
      date = 'Yesterday ' + extra;
    } else if (DateFormat('dd')
            .format(DateTime.fromMillisecondsSinceEpoch(int.parse(d))) ==
        (DateFormat("dd").format(DateTime.now()))) {
      String extra = DateFormat('hh:mm a')
          .format(DateTime.fromMillisecondsSinceEpoch(int.parse(d)));
      date = "Today " + extra;
    } else {
      date = DateFormat('dd MMM y')
          .format(DateTime.fromMillisecondsSinceEpoch(int.parse(d)));
    }
    return date;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.friendName,
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 5),
            Text(
              getDate(widget.sendTime),
              style: TextStyle(fontSize: 12),
            ),
          ],
        ),
      ),
      body: Hero(
        tag: widget.url,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: ExtendedImage.network(
            widget.url,
            fit: BoxFit.contain,
            mode: ExtendedImageMode.gesture,
            initGestureConfigHandler: (state) {
              return GestureConfig(
                minScale: 0.9,
                animationMinScale: 0.7,
                maxScale: 3.0,
                animationMaxScale: 3.5,
                speed: 1.0,
                inertialSpeed: 100.0,
                initialScale: 1.0,
                inPageView: false,
                initialAlignment: InitialAlignment.center,
              );
            },
          ),
        ),
        // PhotoView(
        //   imageProvider: CachedNetworkImageProvider(
        //     widget.url,
        //   ),
        // ),
      ),
    );
  }
}
