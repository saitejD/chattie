import 'package:cached_network_image/cached_network_image.dart';
import 'package:chattie/bloc/chat_bloc.dart';
import 'package:chattie/callscreens/pickup/pickup_layout.dart';
import 'package:chattie/screens/all_contacts_screen.dart';
import 'package:chattie/bloc/chatrooms_bloc.dart';
import 'package:chattie/bloc/user_provider.dart';
import 'package:chattie/screens/chat_screen.dart';
import 'package:chattie/models/last_message_model.dart';
import 'package:chattie/models/user_model.dart';
import 'package:chattie/screens/profile_screen.dart';
import 'package:chattie/services/constants.dart';
import 'package:chattie/services/database_methods.dart';
import 'package:chattie/services/helper_functions.dart';
import 'package:chattie/services/push_notification_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';

import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  TextEditingController _searchController = TextEditingController();

  late ChatRoomsBloc chatRoomsBloc;
  List<LastMessage> futureChatRooms = [];
  List<LastMessage> filteredFutureChatRooms = [];
  bool _isSearching = false;

  late UserProvider userProvider;
  UserModel currentUser = UserModel(
    about: '',
    createdAt: '',
    name: '',
    phoneNo: '',
    imgUrl: '',
    status: '',
    uid: '',
  );

  late PushNotificationService notificationService;

  Widget _buildSearchField() {
    return TextField(
      controller: _searchController,
      textInputAction: TextInputAction.done,
      autofocus: true,
      cursorColor: Colors.white,
      decoration: const InputDecoration(
        hintText: 'Search...',
        border: InputBorder.none,
        hintStyle: const TextStyle(color: Colors.white30),
      ),
      style: const TextStyle(color: Colors.white, fontSize: 16.0),
      onChanged: (String name) {
        chatRoomsBloc = BlocProvider.of<ChatRoomsBloc>(context)
          ..add(StreamSearchEvent(
              currentUserName: currentUser.name, friendName: name));
      },
    );
  }

  Future<PermissionStatus> askPermissions() async {
    PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted) {
      Map<Permission, PermissionStatus> permissionStatus =
          await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ??
          PermissionStatus.restricted;
    } else {
      return permission;
    }
  }

  void onloaded() async {
    await askPermissions();
    currentUser = await DatabaseMethods()
        .getUserByUid(FirebaseAuth.instance.currentUser!.uid);
    chatRoomsBloc = BlocProvider.of<ChatRoomsBloc>(context)
      ..add(StreamChatRoomsEvent(currentUserName: currentUser.name));
    setState(() {});
  }

  void filterChatRooms() {
    List<LastMessage> msgs = [];
    msgs.addAll(futureChatRooms);
    setState(() {
      filteredFutureChatRooms = msgs;
    });
    if (_searchController.text.isNotEmpty) {
      msgs.retainWhere((msg) {
        String searchTerm = _searchController.text.toLowerCase();
        String name = currentUser.name;
        if (name.contains(searchTerm))
          return true;
        else
          return false;
      });
      setState(() {
        filteredFutureChatRooms = msgs;
      });
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    SchedulerBinding.instance.addPostFrameCallback((_) {
      userProvider = Provider.of<UserProvider>(context, listen: false);
      userProvider.refreshUser();
    });
    onloaded();
    _searchController.addListener(() {
      filterChatRooms();
    });

    notificationService = PushNotificationService();
    notificationService.initialFirebaseMessaging();
    // notificationService.sendPushNotification();

    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _searchController.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("state changed");
    if (state == AppLifecycleState.resumed) {
      // online
      DatabaseMethods().setStatus("Online");
    } else {
      //offline
      DatabaseMethods()
          .setStatus(DateTime.now().millisecondsSinceEpoch.toString());
    }
  }

  String getDate(LastMessage document) {
    if (int.parse(DateFormat('dd').format(DateTime.fromMillisecondsSinceEpoch(
            int.parse(document.timeStamp)))) ==
        (int.parse(DateFormat("dd").format(DateTime.now())) - 1)) {
      return 'Yesterday';
    } else if (DateFormat('dd').format(DateTime.fromMillisecondsSinceEpoch(
            int.parse(document.timeStamp))) ==
        (DateFormat("dd").format(DateTime.now()))) {
      return DateFormat('hh:mm a').format(
          DateTime.fromMillisecondsSinceEpoch(int.parse(document.timeStamp)));
    } else {
      return DateFormat('dd MMM').format(
          DateTime.fromMillisecondsSinceEpoch(int.parse(document.timeStamp)));
    }
  }

  List<Widget> searchingActions() {
    return [
      // _searchController.text.isNotEmpty?
      IconButton(
        onPressed: () {
          setState(() {
            _searchController.clear();
            // _isSearching = false;
          });
        },
        icon: Icon(Icons.close),
      )
      // : SizedBox.shrink(),
    ];
  }

  List<Widget> stableActions() {
    return [
      IconButton(
        onPressed: () {
          // BlocProvider.of<ChatRoomsBloc>(context)
          //   ..add(FutureChatRoomsEvent(currentUserName: currentUser.name));
          setState(() {
            // _isSearching = !_isSearching;
          });
        },
        icon: Icon(Icons.search),
      ),
      Padding(
        padding: EdgeInsets.only(right: 8.0),
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(CupertinoPageRoute(
                builder: (context) => ProfileScreen(currentUser: currentUser)));
          },
          child: currentUser.imgUrl.isEmpty
              ? personIconBuild()
              : profilePhoto(currentUser.imgUrl),
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: color1,
        // leading: _isSearching
        //     ? BackButton(
        //         onPressed: () {
        //           BlocProvider.of<ChatRoomsBloc>(context)
        //             ..add(StreamChatRoomsEvent(
        //                 currentUserName: currentUser.name));
        //           setState(() {
        //             _chatRoomsStreamModified = _chatRoomsStreamOriginal;
        //             _isSearching = !_isSearching;
        //           });
        //         },
        //       )
        //     : null,
        title: _isSearching ? _buildSearchField() : Text('Chattie'),
        actions: _isSearching ? searchingActions() : stableActions(),
      ),
      floatingActionButton: FloatingActionButton(
        // backgroundColor: color1,
        onPressed: () {
          Navigator.of(context).push(
              CupertinoPageRoute(builder: (context) => AllContactsScreen()));
        },
        child: Icon(Icons.message_rounded, color: Colors.white),
      ),
      body: Body(
        currentUser: currentUser,
      ),
    );
  }
}

class Body extends StatelessWidget {
  final UserModel currentUser;
  Body({Key? key, required this.currentUser}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatRoomsBloc, ChatRoomsState>(
      builder: (context, state) {
        if (state is InitialChatRoomsState) {
          return circleLoadingAnimation();
        } else if (state is FetchingChatRoomsState) {
          return messageLoading();
        } else if (state is FetchStreamChatRoomsState) {
          return StreamBuilder<List<LastMessage>>(
            stream: state.chatRoomsStream,
            builder: (context, AsyncSnapshot<List<LastMessage>> snapshot) {
              print("data ${snapshot.hasData}");
              if (snapshot.hasData) {
                print("here");
                if (snapshot.data == null || snapshot.data!.length == 0) {
                  return Center(
                    child: Text(
                      "No Friends yet!",
                      style: Theme.of(context).textTheme.headline3,
                    ),
                  );
                }
                return ListView.builder(
                  // physics: NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  itemBuilder: (BuildContext context, int index) {
                    // DocumentSnapshot document = snapshot.data!.docs[index];
                    LastMessage document = snapshot.data![index];
                    String chattingWith = document.id
                        .replaceAll(currentUser.name, "")
                        .replaceAll("_", "");
                    print(
                        " here - ${snapshot.data!.first.userPics[chattingWith]}");
                    String date = getDate(document.timeStamp);
                    return ListTile(
                      onTap: () {
                        Navigator.of(context).push(
                          CupertinoPageRoute(
                            builder: (context) => BlocProvider(
                              create: (context) => ChatBloc(InitialChatState()),
                              child: ChatScreen(
                                  myUserName: currentUser.name,
                                  chatWithUsername: chattingWith),
                            ),
                          ),
                        );
                      },
                      leading:
                          snapshot.data![index].userPics[chattingWith] == null
                              ? personIconBuild(40, 40)
                              : profilePhoto(
                                  snapshot.data![index].userPics[chattingWith]),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              chattingWith,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Text(
                            date,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 12),
                          ),
                        ],
                      ),
                      subtitle: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: document.type == 0
                                ? Text(
                                    document.content,
                                    style: TextStyle(
                                      color: (document.sendBy !=
                                                  currentUser.name &&
                                              !document.isRead)
                                          ? color1
                                          : Colors.grey,
                                      fontWeight: (document.sendBy !=
                                                  currentUser.name &&
                                              !document.isRead)
                                          ? FontWeight.w800
                                          : FontWeight.w400,
                                    ),
                                    // style: Theme.of(context)
                                    //     .textTheme
                                    //     .headline5!
                                    //     .copyWith(color: Colors.grey),
                                    overflow: TextOverflow.ellipsis,
                                  )
                                : Row(
                                    children: [
                                      Icon(
                                          document.type == 1
                                              ? Icons.photo
                                              : Icons.emoji_emotions_outlined,
                                          size: 20,
                                          color: Colors.grey),
                                      SizedBox(width: 3),
                                      Text(
                                        document.type == 1
                                            ? "Photo"
                                            : "Sticker",
                                        style: TextStyle(
                                          color: (document.sendBy !=
                                                      currentUser.name &&
                                                  !document.isRead)
                                              ? color1
                                              : Colors.grey,
                                          fontWeight: (document.sendBy !=
                                                      currentUser.name &&
                                                  !document.isRead)
                                              ? FontWeight.w800
                                              : FontWeight.w400,
                                        ),
                                      )
                                    ],
                                  ),
                          ),
                          (document.sendBy != currentUser.name &&
                                  !document.isRead)
                              ? Container(
                                  width: 40.0,
                                  height: 20.0,
                                  // padding: EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(30.0),
                                      color: Colors.green),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'NEW',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )
                              : SizedBox(
                                  width: 40.0,
                                  height: 20.0,
                                ),
                        ],
                      ),
                    );
                  },
                );
              } else if (snapshot.hasError) {
                return Center(
                  child: Text(
                    "Error while Loading ...",
                    style: Theme.of(context).textTheme.headlineSmall,
                  ),
                );
              } else {
                return Center(
                  child: Text(
                    "No Friends Yet! Add one ...",
                    style: Theme.of(context).textTheme.headlineSmall,
                  ),
                );
              }
            },
          );
        } else {
          return Center(
            child: Text("Error in loading data"),
          );
        }
      },
    );
  }
}
