import 'package:cached_network_image/cached_network_image.dart';
import 'package:chattie/models/user_model.dart';
import 'package:chattie/services/constants.dart';
import 'package:chattie/services/helper_functions.dart';
import 'package:chattie/screens/zoom_photo.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ViewProfileScreen extends StatefulWidget {
  final UserModel user;
  ViewProfileScreen({required this.user});

  @override
  _ViewProfileScreenState createState() => _ViewProfileScreenState();
}

class _ViewProfileScreenState extends State<ViewProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.black,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Hero(
                  tag: "tag",
                  child: GestureDetector(
                    onTap: () {
                      if (widget.user.imgUrl.isNotEmpty)
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ZoomPhotoScreen(
                              url: widget.user.imgUrl,
                              friendName: widget.user.name,
                            ),
                          ),
                        );
                      else {
                        // ScaffoldMessenger.of(context).showSnackBar(
                        //     SnackBar(content: Text("no profile photo")));
                        showToast("no profile photo");
                      }
                    },
                    child: Container(
                      color: Colors.grey,
                      height: MediaQuery.of(context).size.height * 0.6,
                      child: widget.user.imgUrl.isEmpty
                          ? Center(
                              child: Icon(Icons.person,
                                  color: Colors.white, size: 200),
                            )
                          : CachedNetworkImage(
                              placeholder: (context, url) => Center(
                                child:
                                    CircularProgressIndicator(strokeWidth: 1.0),
                              ),
                              errorWidget:
                                  (BuildContext context, String url, error) =>
                                      Image.asset(
                                'assets/images/img_not_available.jpeg',
                                height: MediaQuery.of(context).size.width * 0.6,
                                // height: 200.0,
                              ),
                              imageUrl: widget.user.imgUrl,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                height: MediaQuery.of(context).size.width * 0.6,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                    ),
                  ),
                ),
                Positioned(
                    top: 30, left: 10, child: BackButton(color: Colors.white)),
                Positioned(
                  bottom: 10,
                  left: 20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.user.name,
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      // SizedBox(height: 5),
                      Text(
                        getStatus(widget.user.status),
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.white70,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Container(
              padding: EdgeInsets.all(10),
              // margin: EdgeInsets.all(5),
              color: color1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "About and Phone number",
                    style: TextStyle(
                        color: Colors.white70,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 5),
                  Text(
                    widget.user.about,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  SizedBox(height: 2),
                  Text(
                    DateFormat("dd MMM y").format(
                      DateTime.fromMicrosecondsSinceEpoch(
                          int.parse(widget.user.createdAt.toString())),
                    ),
                    style: TextStyle(color: Colors.white38),
                  ),
                  SizedBox(height: 3),
                  Divider(color: Colors.grey),
                  SizedBox(height: 3),
                  Text(
                    widget.user.phoneNo,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  SizedBox(height: 2),
                  Text(
                    "Mobile",
                    style: TextStyle(color: Colors.white38),
                  ),
                  SizedBox(height: 5),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
