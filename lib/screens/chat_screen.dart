import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:chattie/bloc/chat_bloc.dart';
import 'package:chattie/callscreens/call_screen.dart';
import 'package:chattie/callscreens/pickup/pickup_layout.dart';
import 'package:chattie/models/call.dart';
import 'package:chattie/models/message_model.dart';
import 'package:chattie/models/user_model.dart';
import 'package:chattie/screens/view_photo_message.dart';
import 'package:chattie/screens/view_profile_screen.dart';
import 'package:chattie/services/call_methods.dart';
import 'package:chattie/services/constants.dart';
import 'package:chattie/services/database_methods.dart';
import 'package:chattie/services/helper_functions.dart';
import 'package:chattie/services/permisions_functions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:bubble/bubble.dart';
import 'package:swipe_to/swipe_to.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'package:linkwell/linkwell.dart';
import 'package:http/http.dart' as http;

class ChatScreen extends StatefulWidget {
  final String chatWithUsername, myUserName;
  ChatScreen({
    required this.chatWithUsername,
    required this.myUserName,
  });
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  File? imageFile;
  bool isSendingAttachment = false;
  String imageUrl = '';
  String replyMessage = "";
  String sendBy = "";
  bool swiped = false;
  String chatRoomId = "";
  Stream<UserModel>? friendStream;
  UserModel? friend;
  UserModel? currentUser;
  TextEditingController messageTextEdittingController = TextEditingController();
  FocusNode focusNode = FocusNode();
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  ScrollController _scrollController = ScrollController();
  List<MessageModel> messages = [];
  ImagePicker _picker = ImagePicker();
  late ChatBloc chatBloc;
  String bottomSheetOption = '';
  List<MessageModel> allMessages = [];
  MessageModel? repliedMesage = MessageModel(
      content: "", sendBy: "", timeStamp: "", type: 0, deleted: false);
  MessageModel? swipedMessage = MessageModel(
      content: "", sendBy: "", timeStamp: "", type: 0, deleted: false);
  bool isShowSticker = false;

  // @override
  // bool get wantKeepAlive => true;

  Future<void> getAllMessages() async {
    allMessages = await DatabaseMethods().getAllMessages(chatRoomId);
  }

  doThisOnLaunch() async {
    chatRoomId =
        getChatRoomIdByUsernames(widget.chatWithUsername, widget.myUserName);
    await getAllMessages();
    await updateIsRead();
    friend = await DatabaseMethods().getUserByName(widget.chatWithUsername);
    currentUser = await DatabaseMethods().getUserByName(widget.myUserName);
    friendStream = DatabaseMethods().getFriendStream(widget.chatWithUsername);
    setState(() {});
    print("last ${allMessages.length}");
  }

  updateIsRead() async {
    String chatRoomId =
        getChatRoomIdByUsernames(widget.chatWithUsername, widget.myUserName);
    Map<String, bool> lastMessageInfo = {"isRead": true};
    await DatabaseMethods()
        .updateIsRead(chatRoomId, lastMessageInfo, widget.myUserName);
  }

  @override
  void initState() {
    print("chat user name: ${widget.chatWithUsername}");
    chatBloc = context.read<ChatBloc>()
      ..add(FetchMessagesEvent(
          currentUserName: widget.myUserName,
          chattingWithUserName: widget.chatWithUsername));
    doThisOnLaunch();
    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;
      double delta = MediaQuery.of(context).size.height * 0.20;
      if (currentScroll == maxScroll) {
        BlocProvider.of<ChatBloc>(context).add(FetchPreviousMessagesEvent(
          widget.myUserName,
          widget.chatWithUsername,
          messages.last,
        ));
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
    imageFile = null;
  }

  Future getGalleryImage() async {
    FocusScope.of(context).unfocus();
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
    imageFile = File(pickedFile!.path);
    if (imageFile != null) {
      setState(() {
        isSendingAttachment = true;
      });
      uploadFile();
    }
  }

  Future getCameraImage() async {
    FocusScope.of(context).unfocus();
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.camera);
    imageFile = File(pickedFile!.path);
    if (imageFile != null) {
      setState(() {
        isSendingAttachment = true;
      });
      uploadFile();
    }
  }

  Future uploadFile() async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    Reference reference = FirebaseStorage.instance.ref().child(fileName);
    UploadTask uploadTask = reference.putFile(imageFile!);
    TaskSnapshot storageTaskSnapshot =
        await uploadTask.whenComplete(() => null);
    storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
      imageUrl = downloadUrl;
      setState(() {
        isSendingAttachment = false;
        onSendMessage(imageUrl, 1);
      });
    }, onError: (err) {
      setState(() {
        isSendingAttachment = false;
      });
    });
  }

  void onMessageSwiped(MessageModel msg) {
    // focusNode.requestFocus();
    setState(() {
      swipedMessage = msg;
      // replyMessage = message;
      // sendBy = send;
      swiped = true;
    });
  }

  void onCancelReply() {
    setState(() {
      // replyMessage = "";
      // sendBy = "";
      swipedMessage = MessageModel(
          content: "", sendBy: "", timeStamp: "", type: 0, deleted: false);
      swiped = false;
    });
  }

  String getStatus(String status) {
    if (status == "Online") {
      return "Online";
    } else {
      return timeago.format(
        DateTime.fromMillisecondsSinceEpoch(int.parse(status)),
      );
    }
  }

  void onSendMessage(String content, int type) async {
    if (messages.length == 0) {
      print("no msgs ${messages.length}");
      UserModel currentUser =
          await DatabaseMethods().getUserByName(widget.myUserName);
      UserModel friend =
          await DatabaseMethods().getUserByName(widget.chatWithUsername);
      Map<String, dynamic> chatRoomInfoMap = {
        "users": [currentUser.name, friend.name],
        "usersPics": {
          widget.myUserName: currentUser.imgUrl,
          widget.chatWithUsername: friend.imgUrl,
        },
      };
      await DatabaseMethods().createChatRoom(chatRoomId, chatRoomInfoMap);
    }
    if (content.trim() != '') {
      messageTextEdittingController.clear();
      var lastMessageTs = DateTime.now().millisecondsSinceEpoch.toString();
      MessageModel message = MessageModel(
        content: content,
        sendBy: widget.myUserName,
        timeStamp: lastMessageTs,
        type: type,
        repliedMsgId: swipedMessage!.id,
        deleted: false,
      );
      Map<String, dynamic> lastMessageInfoMap = {
        "lastMessage": content,
        "lastMessageSendTs": lastMessageTs,
        "lastMessageSendBy": widget.myUserName,
        'type': type,
        'isRead': false,
      };
      DatabaseMethods().addMessage(chatRoomId, message.toJson());
      DatabaseMethods().updateLastMessageSend(chatRoomId, lastMessageInfoMap);

      _scrollController.animateTo(0.0,
          duration: Duration(microseconds: 100), curve: Curves.easeOut);
    }
    await getAllMessages();
    onCancelReply();
  }

  Future<dynamic> _showBottom(MessageModel msg) {
    return showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 150,
          // color: Colors.teal[100],
          child: ListView(
            padding: EdgeInsets.all(10),
            children: [
              ListTile(
                onTap: () {
                  Navigator.of(context).pop("copy");
                  showToast("message copied to clipboard");
                },
                leading: CircleAvatar(
                    backgroundColor: Colors.grey.withOpacity(0.2),
                    child: Icon(Icons.copy)),
                title: Text("Copy Message"),
              ),
              SizedBox(height: 5),
              ListTile(
                onTap: () {
                  Navigator.of(context).pop("delete");
                  DatabaseMethods().deleteMessage(chatRoomId, msg.id!);
                  MessageModel message = messages.first;
                  if (message.id == msg.id) {
                    MessageModel lastMessage = messages[1];
                    Map<String, dynamic> lastMessageInfoMap = {
                      "lastMessage": lastMessage.content,
                      "lastMessageSendTs": lastMessage.timeStamp,
                      "lastMessageSendBy": lastMessage.sendBy,
                      'type': lastMessage.type,
                      // 'isRead': true,
                    };
                    DatabaseMethods()
                        .updateLastMessageSend(chatRoomId, lastMessageInfoMap);
                  }
                },
                leading: CircleAvatar(
                    backgroundColor: Colors.grey.withOpacity(0.2),
                    child: Icon(Icons.delete, color: Colors.red)),
                title:
                    Text("Delete Message", style: TextStyle(color: Colors.red)),
              ),
            ],
          ),
        );
      },
    );
  }

  // Hide sticker or back
  Future<bool> onBackPress() {
    if (isShowSticker) {
      setState(() {
        isShowSticker = false;
      });
    } else {
      Navigator.pop(context);
    }
    return Future.value(false);
  }

  void getSticker() {
    // Hide keyboard when sticker appear
    focusNode.unfocus();
    setState(() {
      isShowSticker = !isShowSticker;
    });
  }

  void onFocusChange() {
    if (focusNode.hasFocus) {
      print("in focus chage");
      // Hide sticker when keyboard appear
      setState(() {
        isShowSticker = false;
      });
    }
  }

  Future<String> getTokenFromServer(
      int uid, String channelName, int tokenRole) async {
    String serverUrl =
        "https://agora-token-service-production-6820.up.railway.app";
    String tokenExpireTime = "60";
    
    try {
      String url =
        "$serverUrl/rtc/$channelName/${tokenRole.toString()}/uid/${uid.toString()}?expiry=$tokenExpireTime";
    print("url $url");
      final response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(response.body);
        String newToken = json['rtcToken'];
        print('Token Received: $newToken');
        return newToken;
      }
    } catch (e) {
      print("error in agora token server $e");
    }
    return "";
  }

  makeCall(UserModel from, UserModel to, bool isVideoCall) async {
    String channelId = Random().nextInt(100).toString();
    String token = await getTokenFromServer(0, channelId, 0);
    if (token != "") {
      Call call = Call(
        callerId: from.uid,
        callerName: from.name,
        callerPic: from.imgUrl,
        receiverId: to.uid,
        receiverName: to.name,
        receiverPic: to.imgUrl,
        channelId: channelId,
        hasDialled: true,
        token: token,
        isVideoCall: isVideoCall,
      );
      print("dailer ${from.name} friend ${to.name}");
      bool callMade = await CallMethods().makeCall(call: call);
      call.hasDialled = true;
      if (callMade) {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => CallScreen(call: call)));
      }
    } else {
      print("failed to get token from server");
    }
  }

  @override
  Widget build(BuildContext context) {
    return PickupLayout(
      scaffold: Scaffold(
        appBar: AppBar(
          // backgroundColor: color1,
          title: StreamBuilder<UserModel>(
            stream: friendStream,
            builder: (context, AsyncSnapshot<UserModel> snapshot) {
              if (snapshot.hasData) {
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      CupertinoPageRoute(
                        builder: (context) => ViewProfileScreen(user: friend!),
                      ),
                    );
                  },
                  child: Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      snapshot.data!.imgUrl.isEmpty
                          ? personIconBuild(40, 40)
                          : profilePhoto(snapshot.data!.imgUrl),
                      SizedBox(width: 5),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            snapshot.data!.name,
                            overflow: TextOverflow.ellipsis,
                            // style: Theme.of(context)
                            //     .appBarTheme
                            //     .textTheme!
                            //     .headline5!
                            //     .copyWith(fontSize: 22),
                          ),
                          Text(
                            getStatus(snapshot.data!.status),
                            style: TextStyle(fontSize: 12, color: Colors.white),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              } else {
                return Container();
              }
            },
          ),
          actions: [
            IconButton(
              onPressed: () async =>
                  await Permissions.cameraAndMicrophonePermissionsGranted()
                      ? makeCall(currentUser!, friend!, true)
                      : () {},
              icon: Icon(Icons.video_call),
            ),
            IconButton(
              onPressed: () async =>
                  await Permissions.cameraAndMicrophonePermissionsGranted()
                      ? makeCall(currentUser!, friend!, false)
                      : () {},
              icon: Icon(Icons.phone),
            ),
          ],
        ),
        body: BlocBuilder<ChatBloc, ChatState>(
          builder: (context, state) {
            // bool hasMore = true;
            // print("right $state");
            // if (state is FetchingMessageState) {
            //   return Center(child: CircularProgressIndicator());
            // }
            if (state is FetchedMessagesState) {
              if (state.isPrevious) {
                messages.addAll(state.messages);
              } else {
                messages = state.messages;
              }
            }
            return WillPopScope(
              onWillPop: onBackPress,
              child: Stack(
                children: [
                  Column(
                    children: <Widget>[
                      buildListMessage(messages),
                      // Input content
                      buildInput(),
                      // show strickers
                      (isShowSticker ? buildSticker() : Container()),
                    ],
                  ),
                  if (isSendingAttachment)
                    Container(
                      width: double.infinity,
                      height: double.infinity,
                      color: Colors.white60,
                      child: Center(child: CircularProgressIndicator()),
                    ),
                ],
              ),
            );
            // print("messages ${messages.length}");

            // else if (state is ErrorState)
            //   return Center(child: Text(state.exception.toString()));
            // else if (state is InitialChatState)
            //   return Center(child: CircularProgressIndicator());
            // else
            //   return Center(child: Text("something worng"));
          },
        ),
      ),
    );
  }

  Widget buildSticker() {
    return Container(
      decoration: BoxDecoration(
        border: Border(top: BorderSide(color: Colors.grey, width: 0.5)),
        color: Colors.white,
      ),
      // padding: EdgeInsets.all(5.0),
      // height: 180.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextButton(
                onPressed: () => onSendMessage('mimi1', 2),
                child: Image.asset(
                  'assets/stickers/mimi1.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi2', 2),
                child: Image.asset(
                  'assets/stickers/mimi2.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi3', 2),
                child: Image.asset(
                  'assets/stickers/mimi3.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              TextButton(
                onPressed: () => onSendMessage('mimi4', 2),
                child: Image.asset(
                  'assets/stickers/mimi4.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi5', 2),
                child: Image.asset(
                  'assets/stickers/mimi5.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi6', 2),
                child: Image.asset(
                  'assets/stickers/mimi6.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              TextButton(
                onPressed: () => onSendMessage('mimi7', 2),
                child: Image.asset(
                  'assets/stickers/mimi7.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi8', 2),
                child: Image.asset(
                  'assets/stickers/mimi8.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi9', 2),
                child: Image.asset(
                  'assets/stickers/mimi9.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  Widget buildDate(String date) {
    String day = getDate(date);
    print("buildDate");
    return Bubble(
      color: Colors.blueGrey,
      // padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      margin: BubbleEdges.symmetric(vertical: 10),
      child: Text(
        day,
        style: TextStyle(
          fontWeight: FontWeight.w600,
          color: Colors.white,
        ),
      ),
    );
  }

  Widget repliedMessageContainer(bool isMe, MessageModel msg) {
    // print("sendBy1 $repliedTo ");
    return Container(
      decoration: BoxDecoration(
        color: Colors.black.withOpacity(0.2),
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      padding: EdgeInsets.all(3),
      height: 50,
      width: double.infinity,
      // margin: EdgeInsets.only(bottom: 5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            color: Colors.black,
            width: 4,
          ),
          SizedBox(width: 8),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  msg.sendBy == widget.myUserName ? "You" : msg.sendBy,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.bold,
                        color: isMe ? Colors.white : Colors.black,
                      ),
                ),
                // const SizedBox(height: 8),
                msg.type == 0
                    ? Expanded(
                        child: Text(
                          msg.content,
                          style: TextStyle(
                            color: isMe ? Colors.white70 : Colors.black87,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    // : msg.type == 1?
                    : Row(
                        children: [
                          Icon(
                            msg.type == 1 ? Icons.photo : Icons.emoji_emotions,
                            size: 20,
                            color: isMe ? Colors.white70 : Colors.black,
                          ),
                          SizedBox(width: 2),
                          Text(
                            msg.type == 1 ? "Photo" : "Sticker",
                            style: TextStyle(
                              color: isMe ? Colors.white70 : Colors.black87,
                            ),
                          )
                        ],
                      )
              ],
            ),
          ),
          if (msg.type == 1 || msg.type == 2) SizedBox(width: 10),
          if (msg.type == 1)
            CachedNetworkImage(
              placeholder: (context, url) => Container(
                  width: 50.0,
                  height: 50.0,
                  child: CircularProgressIndicator(strokeWidth: 1.0)),
              errorWidget: (BuildContext context, String url, error) =>
                  Icon(Icons.error),
              imageUrl: msg.content,
              imageBuilder: (context, imageProvider) => Container(
                width: 50.0,
                height: 50.0,
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          if (msg.type == 2)
            Image.asset(
              'assets/stickers/${msg.content}.gif',
              width: 45.0,
              height: 45.0,
              fit: BoxFit.cover,
            )
        ],
      ),
    );
  }

  Widget buildItem(MessageModel message, bool sendByMe) {
    print("status ${message.deleted}");
    MessageModel replyMsg = MessageModel(
        content: "", sendBy: "", timeStamp: "", type: 0, deleted: false);
    if (message.repliedMsgId != null && message.repliedMsgId!.isNotEmpty) {
      List<MessageModel> m = allMessages
          .where((element) => element.id == message.repliedMsgId)
          .toList();
      if (m.isNotEmpty) replyMsg = m.first;
    }
    return Row(
      mainAxisAlignment:
          sendByMe ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: [
        !message.deleted
            ? GestureDetector(
                onLongPress: () async {
                  if (message.sendBy == widget.myUserName) _showBottom(message);
                },
                child: SwipeTo(
                  onRightSwipe: () {
                    print("swiped mes ${message.sendBy}");
                    onMessageSwiped(message);
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: sendByMe
                        ? CrossAxisAlignment.end
                        : CrossAxisAlignment.start,
                    children: [
                      message.type == 0
                          ?
                          // text message
                          ConstrainedBox(
                              constraints: BoxConstraints(
                                minWidth: 120,
                                maxWidth:
                                    MediaQuery.of(context).size.width * 0.8,
                                minHeight: 50,
                              ),
                              child: IntrinsicWidth(
                                child: Bubble(
                                  stick: true,
                                  color: sendByMe ? color1 : color2,
                                  margin: BubbleEdges.symmetric(vertical: 5),
                                  radius: Radius.circular(8),
                                  nip: sendByMe
                                      ? BubbleNip.rightBottom
                                      : BubbleNip.leftBottom,
                                  child: Column(
                                    crossAxisAlignment: sendByMe
                                        ? CrossAxisAlignment.start
                                        : CrossAxisAlignment.start,
                                    children: [
                                      if (message.repliedMsgId != null &&
                                          message.repliedMsgId!.isNotEmpty)
                                        repliedMessageContainer(
                                            sendByMe, replyMsg),
                                      LinkWell(
                                        message.content,
                                        linkStyle: TextStyle(
                                          color: Colors.brown,
                                          fontSize: 16,
                                          decoration: TextDecoration.underline,
                                          // fontStyle: FontStyle.italic,
                                        ),
                                        style: TextStyle(
                                            fontSize: 16,
                                            // height: 1.3,
                                            color: sendByMe
                                                ? Colors.white
                                                : Colors.black),
                                      ),
                                      Row(
                                        mainAxisAlignment: sendByMe
                                            ? MainAxisAlignment.end
                                            : MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            "${DateFormat('jm').format(DateTime.fromMillisecondsSinceEpoch(int.parse(message.timeStamp)))}",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: sendByMe
                                                    ? Colors.white70
                                                    : Colors.black45),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          // photo message
                          : message.type == 1
                              ? ConstrainedBox(
                                  constraints: BoxConstraints(
                                    minWidth:
                                        MediaQuery.of(context).size.width * 0.4,
                                    maxWidth:
                                        MediaQuery.of(context).size.width * 0.8,
                                    // minHeight: 200,
                                    // maxHeight: MediaQuery.of(context).size.height * 0.5,
                                  ),
                                  child: Bubble(
                                    stick: true,
                                    color: sendByMe ? color1 : color2,
                                    margin: BubbleEdges.symmetric(vertical: 5),
                                    radius: Radius.circular(8),
                                    nip: sendByMe
                                        ? BubbleNip.rightBottom
                                        : BubbleNip.leftBottom,
                                    child: Column(
                                      crossAxisAlignment: sendByMe
                                          ? CrossAxisAlignment.start
                                          : CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        if (message.repliedMsgId != null &&
                                            message.repliedMsgId!.isNotEmpty)
                                          repliedMessageContainer(
                                              sendByMe, replyMsg),
                                        Hero(
                                          tag: message.content,
                                          child: GestureDetector(
                                            onTap: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      ViewPhotoMessage(
                                                    url: message.content,
                                                    friendName: message
                                                                .sendBy ==
                                                            widget.myUserName
                                                        ? "You"
                                                        : message.sendBy,
                                                    sendTime: message.timeStamp,
                                                  ),
                                                ),
                                              );
                                            },
                                            child: Stack(
                                              children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  child: CachedNetworkImage(
                                                    placeholder:
                                                        (context, url) =>
                                                            Container(
                                                      height: 200,
                                                      child: Center(
                                                          child:
                                                              CircularProgressIndicator(
                                                        strokeWidth: 1.0,
                                                      )),
                                                    ),
                                                    errorWidget:
                                                        (BuildContext context,
                                                                String url,
                                                                error) =>
                                                            Image.asset(
                                                      'assets/img_not_available.jpeg',
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.7,
                                                      height: 200.0,
                                                      // fit: BoxFit.cover,
                                                    ),
                                                    imageUrl: message.content,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                                Positioned(
                                                  bottom: 5,
                                                  right: 8,
                                                  child: Text(
                                                    "${DateFormat('jm').format(DateTime.fromMillisecondsSinceEpoch(int.parse(message.timeStamp)))}",
                                                    style: TextStyle(
                                                        fontSize: 10,
                                                        color: Colors.white),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              // sticker message
                              : message.repliedMsgId != null &&
                                      message.repliedMsgId!.isNotEmpty
                                  ? ConstrainedBox(
                                      constraints: BoxConstraints(
                                        // minWidth:
                                        //     MediaQuery.of(context).size.width * 0.35,
                                        maxWidth:
                                            MediaQuery.of(context).size.width *
                                                0.4,
                                        // minHeight: 200,
                                        // maxHeight: 250,
                                      ),
                                      child: Bubble(
                                        stick: true,
                                        color: sendByMe ? color1 : color2,
                                        margin:
                                            BubbleEdges.symmetric(vertical: 5),
                                        radius: Radius.circular(8),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            if (message.repliedMsgId != null &&
                                                message
                                                    .repliedMsgId!.isNotEmpty)
                                              repliedMessageContainer(
                                                  sendByMe, replyMsg),
                                            Container(
                                              child: Image.asset(
                                                'assets/stickers/${message.content}.gif',
                                                width: 100.0,
                                                height: 100.0,
                                                fit: BoxFit.cover,
                                              ),
                                              margin: EdgeInsets.only(
                                                top: 5,
                                                // bottom: 5,
                                                left: sendByMe ? 0.0 : 16.0,
                                                right: sendByMe ? 16.0 : 0.0,
                                              ),
                                            ),
                                            Row(
                                              mainAxisAlignment: sendByMe
                                                  ? MainAxisAlignment.end
                                                  : MainAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "${DateFormat('jm').format(DateTime.fromMillisecondsSinceEpoch(int.parse(message.timeStamp)))}",
                                                  style: TextStyle(
                                                      fontSize: 10,
                                                      color: sendByMe
                                                          ? Colors.white70
                                                          : Colors.black45),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  : Column(
                                      crossAxisAlignment: sendByMe
                                          ? CrossAxisAlignment.end
                                          : CrossAxisAlignment.start,
                                      children: [
                                        // if (message.repliedMsgId != null &&
                                        //     message.repliedMsgId!.isNotEmpty)
                                        //   repliedMessageContainer(sendByMe, replyMsg),
                                        Container(
                                          child: Image.asset(
                                            'assets/stickers/${message.content}.gif',
                                            width: 100.0,
                                            height: 100.0,
                                            fit: BoxFit.cover,
                                          ),
                                          margin: EdgeInsets.only(
                                            top: 5,
                                            // bottom: 5,
                                            left: sendByMe ? 0.0 : 16.0,
                                            right: sendByMe ? 16.0 : 0.0,
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            "${DateFormat('jm').format(DateTime.fromMillisecondsSinceEpoch(int.parse(message.timeStamp)))}",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.grey),
                                          ),
                                        ),
                                      ],
                                    ),
                    ],
                  ),
                ),
              )
            : Bubble(
                stick: true,
                color: sendByMe ? color1 : color2,
                margin: BubbleEdges.symmetric(vertical: 5),
                radius: Radius.circular(8),
                nip: sendByMe ? BubbleNip.rightBottom : BubbleNip.leftBottom,
                child: IntrinsicWidth(
                  child: Column(
                    crossAxisAlignment: sendByMe
                        ? CrossAxisAlignment.start
                        : CrossAxisAlignment.start,
                    children: [
                      Text(
                        "This message was deleted",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 16,
                            fontStyle: FontStyle.italic,
                            color: sendByMe ? Colors.white : Colors.black),
                      ),
                      Row(
                        mainAxisAlignment: sendByMe
                            ? MainAxisAlignment.end
                            : MainAxisAlignment.start,
                        children: [
                          Text(
                            "${DateFormat('jm').format(DateTime.fromMillisecondsSinceEpoch(int.parse(message.timeStamp)))}",
                            style: TextStyle(
                                fontSize: 10,
                                color:
                                    sendByMe ? Colors.white70 : Colors.black45),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
      ],
    );
  }

  Widget buildReplyingToMessageContainer(String message, String send) =>
      Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.2),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12),
            topRight: Radius.circular(12),
          ),
        ),
        child: IntrinsicHeight(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5))),
            child: Row(
              children: [
                Container(
                  color: Colors.green,
                  width: 4,
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Text(
                              send == widget.myUserName ? "You" : send,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                          ),
                          GestureDetector(
                            child: Icon(Icons.close, size: 18),
                            onTap: onCancelReply,
                          )
                        ],
                      ),
                      const SizedBox(height: 3),
                      Text(
                        message,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );

  Widget buildInput() {
    print(swiped);
    if (swiped) focusNode.requestFocus();
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              children: [
                if (swiped)
                  ReplyMessageWidget(
                    message: swipedMessage!,
                    currentUserName: widget.myUserName,
                    onCancelReply: onCancelReply,
                  ),
                Container(
                  constraints: new BoxConstraints(
                    minHeight: 50.0,
                    maxHeight: 130.0,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.2),
                    borderRadius: BorderRadius.only(
                      topLeft: swiped ? Radius.zero : Radius.circular(30),
                      topRight: swiped ? Radius.zero : Radius.circular(30),
                      bottomLeft:
                          swiped ? Radius.circular(10) : Radius.circular(30),
                      bottomRight:
                          swiped ? Radius.circular(10) : Radius.circular(30),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(width: 5),
                      InkWell(
                        child: Icon(
                          Icons.emoji_emotions_outlined,
                          size: 25,
                          color: Colors.grey[700],
                        ),
                        onTap: () {
                          if (!isShowSticker) {
                            focusNode.unfocus();
                            setState(() {
                              isShowSticker = true;
                            });
                          } else {
                            focusNode.requestFocus();
                            setState(() {
                              isShowSticker = false;
                            });
                          }
                        },
                      ),
                      Flexible(
                        child: new Scrollbar(
                          child: new TextField(
                            focusNode: focusNode,
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            onTap: () => setState(() {
                              isShowSticker = false;
                            }),
                            controller: messageTextEdittingController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.only(
                                  topLeft: swiped
                                      ? Radius.zero
                                      : Radius.circular(24),
                                  topRight: swiped
                                      ? Radius.zero
                                      : Radius.circular(24),
                                  bottomLeft: Radius.circular(24),
                                  bottomRight: Radius.circular(24),
                                ),
                              ),
                              contentPadding: EdgeInsets.only(
                                top: 2.0,
                                left: 5.0,
                                right: 0.0,
                                bottom: 2.0,
                              ),
                              hintText: "Type your message...",
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(fontSize: 16),
                          ),
                        ),
                      ),
                      InkWell(
                        child: Icon(
                          Icons.photo_camera,
                          size: 25,
                          color: Colors.grey[700],
                        ),
                        onTap: getCameraImage,
                      ),
                      SizedBox(width: 5),
                      InkWell(
                        child: Icon(
                          Icons.photo,
                          size: 25,
                          color: Colors.grey[700],
                        ),
                        onTap: getGalleryImage,
                      ),
                      SizedBox(width: 10),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: 5),
          Container(
            padding: const EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: color1,
            ),
            child: InkWell(
              onTap: () {
                if (messageTextEdittingController.text.length != 0)
                  onSendMessage(messageTextEdittingController.text, 0);
              },
              child: Icon(Icons.send, size: 25, color: Colors.white),
            ),
          )
        ],
      ),
    );
  }

  Widget buildListMessage(List<MessageModel> messages) {
    return Flexible(
        child: ListView.builder(
      reverse: true,
      itemCount: messages.length,
      controller: _scrollController,
      padding: EdgeInsets.all(10.0),
      itemBuilder: (context, index) {
        Widget separator = SizedBox();
        if (messages.length == 1) {
          separator = buildDate(messages[index].timeStamp);
        } else if (messages.length > 1 &&
            index != messages.length - 2 &&
            index != messages.length - 1 &&
            DateFormat('dd').format(DateTime.fromMillisecondsSinceEpoch(
                    int.parse(messages[index].timeStamp))) !=
                DateFormat('dd').format(DateTime.fromMillisecondsSinceEpoch(
                    int.parse(messages[index + 1].timeStamp)))) {
          separator = buildDate(messages[index].timeStamp);
        } else if (messages.length > 1 &&
            index == messages.length - 2 &&
            DateFormat('dd').format(DateTime.fromMillisecondsSinceEpoch(
                    int.parse(messages[index].timeStamp))) !=
                DateFormat('dd').format(DateTime.fromMillisecondsSinceEpoch(
                    int.parse(messages[index + 1].timeStamp)))) {
          separator = buildDate(messages[index].timeStamp);
        } else if (messages.length > 1 && index == messages.length - 1) {
          separator = buildDate(messages[index].timeStamp);
        }
        // DocumentSnapshot ds = messages[index];
        return Column(
          children: [
            separator,
            buildItem(
                messages[index], widget.myUserName == messages[index].sendBy),
          ],
        );
      },
    ));
  }
}

class ReplyMessageWidget extends StatelessWidget {
  final MessageModel message;
  final String currentUserName;
  final VoidCallback onCancelReply;

  const ReplyMessageWidget({
    required this.message,
    required this.onCancelReply,
    required this.currentUserName,
  });

  @override
  Widget build(BuildContext context) {
    // print("sendBy $sentBy");
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.2),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
        ),
      ),
      child: IntrinsicHeight(
        child: Container(
          decoration: BoxDecoration(
              // color: Colors.white.withOpacity(0.7),
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                color: color1,
                width: 4,
              ),
              SizedBox(width: 8),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            message.sendBy.isNotEmpty &&
                                    message.sendBy == currentUserName
                                ? "You"
                                : message.sendBy,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(height: 3),
                          message.type == 1 || message.type == 2
                              ? Row(
                                  children: [
                                    Icon(
                                      message.type == 1
                                          ? Icons.photo
                                          : Icons.emoji_emotions_outlined,
                                      size: 20,
                                      color: Colors.grey,
                                    ),
                                    SizedBox(width: 2),
                                    Text(
                                      message.type == 1 ? "Photo" : "Sticker",
                                      style: TextStyle(
                                        color: Colors.black87,
                                      ),
                                    )
                                  ],
                                )
                              : Text(
                                  message.content,
                                  style: TextStyle(color: Colors.black54),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                        ],
                      ),
                    ),
                    Stack(
                      children: [
                        message.type == 1
                            ? CachedNetworkImage(
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(strokeWidth: 1.0),
                                errorWidget:
                                    (BuildContext context, String url, error) =>
                                        Icon(Icons.error),
                                imageUrl: message.content,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  width: 45.0,
                                  height: 45.0,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              )
                            : message.type == 2
                                ? Image.asset(
                                    'assets/stickers/${message.content}.gif',
                                    width: 45.0,
                                    height: 45.0,
                                    fit: BoxFit.cover,
                                  )
                                : Container(
                                    width: 45.0,
                                    height: 45.0,
                                  ),
                        Positioned(
                          right: 1,
                          top: 0,
                          child: GestureDetector(
                            child: Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white.withOpacity(0.7),
                                ),
                                child: Icon(Icons.close, size: 18)),
                            onTap: onCancelReply,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
