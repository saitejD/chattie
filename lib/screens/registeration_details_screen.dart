import 'dart:io';
import 'package:chattie/bloc/chatrooms_bloc.dart';
import 'package:chattie/screens/home_screen.dart';
import 'package:chattie/services/constants.dart';
import 'package:chattie/services/helper_functions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class RegisterDetailsScreen extends StatefulWidget {
  @override
  _RegisterDetailsScreenState createState() => _RegisterDetailsScreenState();
}

class _RegisterDetailsScreenState extends State<RegisterDetailsScreen> {
  static CroppedFile? _profileImage;
  TextEditingController nameController = TextEditingController();
  TextEditingController aboutController =
      TextEditingController(text: "Hey there! I am on Chattie");
  FirebaseAuth _auth = FirebaseAuth.instance;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final ImagePicker picker = ImagePicker();
  bool isLoading = false;

  /// Crop Image
  _cropImage(String filePath) async {
    CroppedFile? croppedImage = await ImageCropper().cropImage(
      sourcePath: filePath,
      maxWidth: 1080,
      maxHeight: 1080,
      cropStyle: CropStyle.circle,
      aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
      uiSettings: [
        AndroidUiSettings(
          toolbarTitle: 'Cropper',
          initAspectRatio: CropAspectRatioPreset.original,
          cropGridColor: Colors.white,
          statusBarColor: Colors.transparent,
          toolbarColor: Colors.white,
          dimmedLayerColor: Colors.white,
          activeControlsWidgetColor: Colors.white,
          lockAspectRatio: false,
        ),
        IOSUiSettings(
          minimumAspectRatio: 1.0,
        ),
      ],
    );
    if (croppedImage != null) {
      setState(() {
        _profileImage = croppedImage;
      });
    }
  }

  void dispose() {
    _profileImage = null;
    nameController.dispose();
    aboutController.dispose();
    super.dispose();
  }

  _imageFromGalery() async {
    XFile? image =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 100);
    _cropImage(image!.path);
  }

  _imageFromCamer() async {
    final XFile? image =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 100);
    _cropImage(image!.path);
  }

  Future<dynamic> imagePickerBottomSheet(BuildContext context) {
    return showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
            child: Wrap(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      height: 5,
                      width: 40,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(10),
                      )),
                ),
                SizedBox(height: 20),
                Text(
                  "Choose from",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        SizedBox(height: 20),
                        CircleAvatar(
                          radius: 25,
                          backgroundColor: color1,
                          child: IconButton(
                            icon: Icon(
                              Icons.photo_size_select_actual,
                              size: 25,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.pop(context, "gallery");
                              _imageFromGalery();
                            },
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Gallery",
                          style: TextStyle(
                            // color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(width: 50),
                    Column(
                      children: <Widget>[
                        SizedBox(height: 20),
                        CircleAvatar(
                          radius: 25,
                          backgroundColor: color1,
                          child: IconButton(
                            icon: Icon(
                              Icons.camera_alt,
                              size: 28,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.pop(context, "camera");
                              _imageFromCamer();
                            },
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Camera",
                          style: TextStyle(
                            // color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<String> uploadFile() async {
    if (_profileImage == null) return "";
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    Reference reference = FirebaseStorage.instance.ref().child(fileName);
    UploadTask uploadTask = reference.putFile(File(_profileImage!.path));
    TaskSnapshot storageTaskSnapshot =
        await uploadTask.whenComplete(() => null);
    String url = await storageTaskSnapshot.ref.getDownloadURL();
    return url;
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Create Profile"),
      ),
      body: Stack(
        children: [
          Container(
            height: size.height,
            width: size.width,
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Column(
              children: [
                SizedBox(height: 20),
                Stack(
                  children: [
                    _profileImage != null
                        ? Container(
                            width: 180,
                            height: 180,
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.black.withOpacity(0.4),
                                  width: 2),
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: FileImage(File(_profileImage!.path)),
                                  fit: BoxFit.cover),
                            ),
                          )
                        : Container(
                            width: 180,
                            height: 180,
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.black.withOpacity(0.4),
                                  width: 2),
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: AssetImage("assets/person_icon.jpeg"),
                                  fit: BoxFit.cover),
                            ),
                          ),
                    Positioned(
                      bottom: 5,
                      right: 5,
                      child: CircleAvatar(
                        radius: 25,
                        backgroundColor: color1,
                        child: InkWell(
                          onTap: () async {
                            var res = await imagePickerBottomSheet(context);
                            print("here: $res");
                            if (res == "gallery") {
                              _imageFromGalery();
                            } else if (res == 'camera') {
                              _imageFromCamer();
                            }
                          },
                          child: Icon(
                            Icons.camera_alt,
                            color: Colors.white,
                            size: 30,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 40),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(Icons.person, size: 35),
                          SizedBox(width: 10),
                          Expanded(
                            child: TextFormField(
                              controller: nameController,
                              validator: (value) {
                                if (value!.isEmpty)
                                  return "name can't be empty";
                                else
                                  return null;
                              },
                              decoration: InputDecoration(
                                hintText: "Enter Full Name",
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(Icons.info_outlined, size: 30),
                          SizedBox(width: 10),
                          Expanded(
                            child: TextFormField(
                              controller: aboutController,
                              validator: (value) {
                                if (value!.isEmpty)
                                  return "about can't be empty";
                                else
                                  return null;
                              },
                              // initialValue: "Hey there, I am on Chattie",
                              decoration: InputDecoration(
                                hintText: "Enter About",
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                // Spacer(flex: 4),
                // Spacer(),
              ],
            ),
          ),
          if (isLoading) Center(child: threeBounceLoadingAnimation()),
        ],
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ElevatedButton(
          // style: ElevatedButton.styleFrom(
          //   minimumSize: Size(double.infinity, 56),
          //   shape: RoundedRectangleBorder(
          //     borderRadius: BorderRadius.circular(10),
          //   ),
          // ),
          onPressed: () async {
            if (_formKey.currentState!.validate()) {
              if (_auth.currentUser != null) {
                setState(() {
                  isLoading = true;
                });
                // Check is already sign up
                final QuerySnapshot result = await FirebaseFirestore.instance
                    .collection('users')
                    .where('uid', isEqualTo: _auth.currentUser!.uid)
                    .get();
                final List<DocumentSnapshot> documents = result.docs;
                if (documents.length == 0) {
                  String url = await uploadFile();
                  Map<String, dynamic> userInfoMap = {
                    "phoneNo": _auth.currentUser!.phoneNumber,
                    "name": nameController.text,
                    "about": aboutController.text.trim(),
                    "imgUrl": url,
                    "uid": _auth.currentUser!.uid,
                    "status": "Online",
                    "createdAt": DateTime.now().millisecondsSinceEpoch.toString(),
                  };
                  FirebaseFirestore.instance
                      .collection("users")
                      .doc(_auth.currentUser!.uid)
                      .set(userInfoMap);
                  print(_auth.currentUser!.uid);
                  setState(() {
                    isLoading = false;
                  });
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => BlocProvider(
                        create: (context) =>
                            ChatRoomsBloc(InitialChatRoomsState()),
                        child: HomeScreen(),
                      ),
                    ),
                    (route) => false,
                  );
                }
              } else {
                print("user null");
              }
            } else {
              setState(() {
                isLoading = false;
              });
              // ScaffoldMessenger.of(context).showSnackBar(
              //   SnackBar(content: Text("Please fille the above feilds")),
              // );
              showToast("Please fille the above feilds");
            }
          },
          child: Text(
            "Register Account",
            style: TextStyle(fontSize: 22),
          ),
        ),
      ),
    );
  }
}
