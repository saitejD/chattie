import 'dart:async';
import 'package:chattie/screens/home_screen.dart';
import 'package:chattie/screens/registeration_details_screen.dart';
import 'package:chattie/services/constants.dart';
import 'package:chattie/services/helper_functions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/chatrooms_bloc.dart';

class OtpVerficationScreen extends StatefulWidget {
  final String countryCode;
  final String phoneNo;

  OtpVerficationScreen({required this.phoneNo, required this.countryCode});

  @override
  _OtpVerficationScreenState createState() => _OtpVerficationScreenState();
}

class _OtpVerficationScreenState extends State<OtpVerficationScreen> {
  FocusNode pin2FocusNode = FocusNode();
  FocusNode pin3FocusNode = FocusNode();
  FocusNode pin4FocusNode = FocusNode();
  FocusNode pin5FocusNode = FocusNode();
  FocusNode pin6FocusNode = FocusNode();

  TextEditingController controller1 = TextEditingController();
  TextEditingController controller2 = TextEditingController();
  TextEditingController controller3 = TextEditingController();
  TextEditingController controller4 = TextEditingController();
  TextEditingController controller5 = TextEditingController();
  TextEditingController controller6 = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  FirebaseAuth _auth = FirebaseAuth.instance;
  String _verificationCode = '';
  Timer? _timer;
  int _startTime = 60;
  bool isLoading = false;

  // TextEditingController _otpController = TextEditingController();
  // final TextEditingController _pinPutController = TextEditingController();

  // final FocusNode _pinPutFocusNode = FocusNode();

  void phoneSignin(BuildContext context) {
    print("in phone signin top");
    String phoneNo = widget.countryCode + widget.phoneNo;
    _auth.verifyPhoneNumber(
      phoneNumber: phoneNo,
      timeout: Duration(seconds: 60),
      verificationCompleted: (PhoneAuthCredential credentials) async {
        UserCredential userCredential =
            await _auth.signInWithCredential(credentials);
        final user = userCredential.user;
        if (user != null) {
          try {
            final QuerySnapshot result = await FirebaseFirestore.instance
                .collection('users')
                .where('uid', isEqualTo: _auth.currentUser!.uid)
                .get();
            if (result.docs.length > 0) {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => BlocProvider(
                    create: (context) => ChatRoomsBloc(InitialChatRoomsState()),
                    child: HomeScreen(),
                  ),
                ),
                (route) => false,
              );
            } else {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) => RegisterDetailsScreen()),
                (route) => false,
              );
            }
          } catch (e) {
            FocusScope.of(context).unfocus();
            showToast("error in verfication try again!");
          }
        }
      },
      verificationFailed: (FirebaseAuthException exception) {
        print(exception);
      },
      codeSent: (String verificationId, [int? forceResendingToken]) async {
        print("otp $verificationId");
        setState(() {
          _verificationCode = verificationId;
        });
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        setState(() {
          _verificationCode = verificationId;
          timer();
        });
      },
    );
  }

  @override
  void initState() {
    timer();
    phoneSignin(context);
    super.initState();
  }

  void nextField(String value, FocusNode focusNode) {
    if (value.length == 1) {
      focusNode.requestFocus();
    }
  }

  void timer() {
    _startTime = 60;
    if (_timer != null) _timer!.cancel();
    _timer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
      setState(() {
        if (_startTime > 0)
          _startTime--;
        else {
          _timer!.cancel();
          // ScaffoldMessenger.of(context).showSnackBar(
          //   SnackBar(
          //     content: Text("Your OTP expired"),
          //   ),
          // );
          showToast("Your OTP expired");
        }
      });
    });
  }

  @override
  void dispose() {
    pin2FocusNode.dispose();
    pin3FocusNode.dispose();
    pin4FocusNode.dispose();
    pin5FocusNode.dispose();
    pin6FocusNode.dispose();
    _timer!.cancel();
    super.dispose();
  }

  InputDecoration inputDecoration = InputDecoration(
    contentPadding: EdgeInsets.symmetric(vertical: 15),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(15),
      borderSide: BorderSide(color: Colors.black),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(15),
      borderSide: BorderSide(color: Colors.black),
    ),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(15),
      borderSide: BorderSide(color: Colors.black),
    ),
  );

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 50),
                  Text(
                    'OTP Verification',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 5),
                  Text(
                    "We've sent your otp code to ${widget.phoneNo}",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                    maxLines: 1,
                  ),
                  SizedBox(height: 30),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "This code expires in ",
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16),
                      ),
                      Text(
                        "00:$_startTime sec",
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: color1,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Form(
                    key: _formKey,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: 50,
                          child: TextFormField(
                            controller: controller1,
                            autofocus: true,
                            onChanged: (value) {
                              nextField(value, pin2FocusNode);
                            },
                            obscureText: true,
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 24),
                            decoration: inputDecoration,
                          ),
                        ),
                        SizedBox(
                          width: 50,
                          child: TextFormField(
                            controller: controller2,
                            onChanged: (value) {
                              nextField(value, pin3FocusNode);
                            },
                            obscureText: true,
                            focusNode: pin2FocusNode,
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 24),
                            decoration: inputDecoration,
                          ),
                        ),
                        SizedBox(
                          width: 50,
                          child: TextFormField(
                              controller: controller3,
                              focusNode: pin3FocusNode,
                              onChanged: (value) {
                                nextField(value, pin4FocusNode);
                              },
                              obscureText: true,
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 24),
                              decoration: inputDecoration),
                        ),
                        SizedBox(
                          width: 50,
                          child: TextFormField(
                            controller: controller4,
                            onChanged: (value) {
                              nextField(value, pin5FocusNode);
                            },
                            obscureText: true,
                            focusNode: pin4FocusNode,
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 24),
                            decoration: inputDecoration,
                          ),
                        ),
                        SizedBox(
                          width: 50,
                          child: TextFormField(
                              controller: controller5,
                              focusNode: pin5FocusNode,
                              onChanged: (value) {
                                nextField(value, pin6FocusNode);
                              },
                              obscureText: true,
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 24),
                              decoration: inputDecoration),
                        ),
                        SizedBox(
                          width: 50,
                          child: TextFormField(
                            controller: controller6,
                            focusNode: pin6FocusNode,
                            onChanged: (value) {
                              pin6FocusNode.unfocus();
                            },
                            obscureText: true,
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 24),
                            decoration: inputDecoration,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 50),
                  // GestureDetector(
                  //   onTap: () async {
                  //     setState(() {
                  //       // timer();
                  //     });
                  //   },
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.end,
                  //     children: [
                  //       Text(
                  //         'Resend OTP',
                  //         style: TextStyle(
                  //           fontSize: 16,
                  //           fontWeight: FontWeight.bold,
                  //           decoration: TextDecoration.underline,
                  //           color: color2,
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  // SizedBox(height: 50),
                  Spacer(),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size(double.infinity, 56),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    onPressed: () async {
                      String otpCode = controller1.text +
                          controller2.text +
                          controller3.text +
                          controller4.text +
                          controller5.text +
                          controller6.text;
                      print(
                          "verificationId: $_verificationCode and otp: $otpCode");
                      if (otpCode.length == 6) {
                        setState(() {
                          isLoading = true;
                        });
                        try {
                          UserCredential userCredential =
                              await FirebaseAuth.instance.signInWithCredential(
                            PhoneAuthProvider.credential(
                              verificationId: _verificationCode,
                              smsCode: otpCode,
                            ),
                          );
                          if (userCredential.user != null) {
                            final QuerySnapshot result = await FirebaseFirestore
                                .instance
                                .collection('users')
                                .where('uid', isEqualTo: _auth.currentUser!.uid)
                                .get();
                            setState(() {
                              isLoading = false;
                            });
                            if (result.docs.length > 0) {
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      BlocProvider(
                                    create: (context) =>
                                        ChatRoomsBloc(InitialChatRoomsState()),
                                    child: HomeScreen(),
                                  ),
                                ),
                                (route) => false,
                              );
                            } else {
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        RegisterDetailsScreen()),
                                (route) => false,
                              );
                            }
                          }
                          setState(() {
                            isLoading = false;
                          });
                        } catch (e) {
                          // FocusScope.of(context).unfocus();
                          setState(() {
                            isLoading = false;
                          });
                          showToast(e.toString());
                        }
                      } else {
                        // ScaffoldMessenger.of(context).showSnackBar(
                        //   SnackBar(
                        //     content: Text("Invalid OTP"),
                        //   ),
                        // );
                        showToast("Invalid OTP");
                      }
                    },
                    child: Text(
                      "Verify",
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
            if (isLoading) Center(child: threeBounceLoadingAnimation())
          ],
        ),
      ),
    );
  }
}


// PinPut(
//               fieldsCount: 6,
//               textStyle: const TextStyle(fontSize: 25.0, color: Colors.white),
//               eachFieldWidth: 40.0,
//               eachFieldHeight: 55.0,
//               focusNode: _pinPutFocusNode,
//               controller: _pinPutController,
//               submittedFieldDecoration: BoxDecoration(
//                 color: const Color.fromRGBO(43, 46, 66, 1),
//                 borderRadius: BorderRadius.circular(10.0),
//                 border: Border.all(
//                   color: const Color.fromRGBO(126, 203, 224, 1),
//                 ),
//               ),
//               selectedFieldDecoration: BoxDecoration(
//                 color: const Color.fromRGBO(43, 46, 66, 1),
//                 borderRadius: BorderRadius.circular(10.0),
//                 border: Border.all(
//                   color: const Color.fromRGBO(126, 203, 224, 1),
//                 ),
//               ),
//               followingFieldDecoration: BoxDecoration(
//                 color: const Color.fromRGBO(43, 46, 66, 1),
//                 borderRadius: BorderRadius.circular(10.0),
//                 border: Border.all(
//                   color: const Color.fromRGBO(126, 203, 224, 1),
//                 ),
//               ),
//               pinAnimationType: PinAnimationType.fade,
//               onSubmit: (pin) async {
//                 try {
//                   print("otp: $_verificationCode");
//                   UserCredential userCredential = await FirebaseAuth.instance
//                       .signInWithCredential(PhoneAuthProvider.credential(
//                           verificationId: _verificationCode,
//                           smsCode: _otpController.text.trim()));

//                   if (userCredential.user != null) {
//                     Navigator.pushAndRemoveUntil(
//                       context,
//                       MaterialPageRoute(builder: (context) => HomeScreen()),
//                       (route) => false,
//                     );
//                   }
//                 } catch (e) {
//                   FocusScope.of(context).unfocus();
//                   ScaffoldMessenger.of(context).showSnackBar(
//                     SnackBar(
//                       content: Text(e.toString()),
//                     ),
//                   );
//                 }
//               },
//             ),