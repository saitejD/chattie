import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:chattie/bloc/chatrooms_bloc.dart';
import 'package:chattie/screens/login_screen.dart';
import 'package:chattie/models/user_model.dart';
import 'package:chattie/services/constants.dart';
import 'package:chattie/services/database_methods.dart';
import 'package:chattie/services/helper_functions.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:chattie/screens/home_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class ProfileScreen extends StatefulWidget {
  final UserModel currentUser;

  const ProfileScreen({Key? key, required this.currentUser}) : super(key: key);

  @override
  ProfileScreenState createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  static CroppedFile? _profileImage;
  late TextEditingController nameController;
  late TextEditingController aboutController;
  FirebaseAuth _auth = FirebaseAuth.instance;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final ImagePicker picker = ImagePicker();
  bool isLoading = false;

  /// Crop Image
  _cropImage(String filePath) async {
    CroppedFile? croppedImage = await ImageCropper().cropImage(
      sourcePath: filePath,
      maxWidth: 1080,
      maxHeight: 1080,
      cropStyle: CropStyle.circle,
      aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
      uiSettings: [
        AndroidUiSettings(
          toolbarTitle: 'Cropper',
          initAspectRatio: CropAspectRatioPreset.original,
          cropGridColor: Colors.white,
          statusBarColor: Colors.transparent,
          toolbarColor: Colors.white,
          dimmedLayerColor: Colors.white,
          activeControlsWidgetColor: Colors.white,
          lockAspectRatio: false,
        ),
        IOSUiSettings(
          minimumAspectRatio: 1.0,
        ),
      ],
    );
    if (croppedImage != null) {
      setState(() {
        _profileImage = croppedImage;
      });
    }
  }

  _imageFromGalery() async {
    XFile? image =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 100);
    _cropImage(image!.path);
  }

  _imageFromCamer() async {
    final XFile? image =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 100);
    _cropImage(image!.path);
  }

  Future<dynamic> imagePickerBottomSheet(BuildContext context) {
    return showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
            child: Wrap(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      height: 5,
                      width: 40,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(10),
                      )),
                ),
                SizedBox(height: 20),
                Text(
                  "Choose from",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        SizedBox(height: 20),
                        CircleAvatar(
                          radius: 25,
                          backgroundColor: color1,
                          child: IconButton(
                            icon: Icon(
                              Icons.photo_size_select_actual,
                              size: 25,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.pop(context, "gallery");
                              _imageFromGalery();
                            },
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Gallery",
                          style: TextStyle(
                            // color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(width: 50),
                    Column(
                      children: <Widget>[
                        SizedBox(height: 20),
                        CircleAvatar(
                          radius: 25,
                          backgroundColor: color1,
                          child: IconButton(
                            icon: Icon(
                              Icons.camera_alt,
                              size: 28,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.pop(context, "camera");
                              _imageFromCamer();
                            },
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Camera",
                          style: TextStyle(
                            // color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<String> uploadFile() async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    Reference reference = FirebaseStorage.instance.ref().child(fileName);
    UploadTask uploadTask = reference.putFile(File(_profileImage!.path));
    TaskSnapshot storageTaskSnapshot =
        await uploadTask.whenComplete(() => null);
    String url = await storageTaskSnapshot.ref.getDownloadURL();
    return url;
  }

  @override
  void initState() {
    nameController = TextEditingController(text: widget.currentUser.name);
    aboutController = TextEditingController(text: widget.currentUser.about);
    super.initState();
  }

  @override
  void dispose() {
    _profileImage = null;
    super.dispose();
  }

  Widget personIconBuild([double w = 35, double h = 35]) {
    return Container(
      width: w,
      height: h,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.grey,
      ),
      child: Icon(
        Icons.person,
        size: 200,
        color: Colors.white,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
        actions: [
          IconButton(
            onPressed: () async {
              if (aboutController.text.isNotEmpty) {
                setState(() {
                  isLoading = true;
                });
                Map<String, dynamic> user;
                if (_profileImage != null) {
                  String url = await uploadFile();
                  user = {
                    "name": nameController.text.trim(),
                    "about": aboutController.text.trim(),
                    "imgUrl": url,
                  };
                } else {
                  user = {
                    "name": nameController.text.trim(),
                    "about": aboutController.text.trim(),
                  };
                }
                await DatabaseMethods()
                    .updateUserData(widget.currentUser.uid, user);
                setState(() {
                  isLoading = false;
                });
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BlocProvider(
                            create: (context) =>
                                ChatRoomsBloc(InitialChatRoomsState()),
                            child: HomeScreen(),
                          )),
                  (route) => false,
                );
              } else {
                // ScaffoldMessenger.of(context).showSnackBar(
                //     SnackBar(content: Text("about can't be empty")));
                showToast("about can't be empty");
              }
            },
            icon: Icon(Icons.done, size: 30),
          ),
          SizedBox(width: 10)
          // IconButton(
          //   onPressed: () async {
          //     try {
          //       await FirebaseAuth.instance.signOut();
          //     } catch (e) {
          //       print(e);
          //     }
          //     Navigator.of(context).pushAndRemoveUntil(
          //       CupertinoPageRoute(
          //         builder: (BuildContext context) => LoginScreen(),
          //       ),
          //       (route) => false,
          //     );
          //   },
          //   icon: Icon(Icons.exit_to_app),
          // )
        ],
      ),
      body: SafeArea(
        child: Stack(
          alignment: Alignment.center,
          children: [
            SingleChildScrollView(
              child: Container(
                height: size.height,
                width: size.width,
                padding: EdgeInsets.only(top: 20, left: 20, right: 30),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        _profileImage != null
                            ? Container(
                                width: 180,
                                height: 180,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black.withOpacity(0.4),
                                      width: 2),
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image:
                                          FileImage(File(_profileImage!.path)),
                                      fit: BoxFit.cover),
                                ),
                              )
                            : widget.currentUser.imgUrl.isEmpty
                                ? personIconBuild(200, 200)
                                : CachedNetworkImage(
                                    placeholder: (context, url) =>
                                        personIconBuild(200, 200),
                                    errorWidget: (BuildContext context,
                                            String url, error) =>
                                        Icon(Icons.error),
                                    imageUrl: widget.currentUser.imgUrl,
                                    // fit: BoxFit.contain,
                                    imageBuilder: (context, imageProvider) =>
                                        Container(
                                      width: 200,
                                      height: 200.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                    ),
                                  ),
                        Positioned(
                          bottom: 10,
                          right: 5,
                          child: CircleAvatar(
                            radius: 25,
                            backgroundColor: color1,
                            child: InkWell(
                              onTap: () async {
                                String res =
                                    await imagePickerBottomSheet(context);
                                print("here: $res");
                                if (res == "gallery") {
                                  _imageFromGalery();
                                } else if (res == 'camera') {
                                  _imageFromCamer();
                                }
                              },
                              child: Icon(
                                Icons.camera_alt,
                                color: Colors.white,
                                size: 30,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 40),
                    Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Icon(Icons.person, size: 35),
                              SizedBox(width: 20),
                              Expanded(
                                child: TextFormField(
                                  controller: nameController,
                                  readOnly: true,
                                  validator: (value) {
                                    if (value!.isEmpty)
                                      return "name can't be empty";
                                    else
                                      return null;
                                  },
                                  decoration: InputDecoration(
                                    hintText: "Enter Full Name",
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Icon(Icons.info_outlined, size: 30),
                              SizedBox(width: 20),
                              Expanded(
                                child: TextFormField(
                                  controller: aboutController,
                                  // validator: (value) {
                                  //   if (value!.isEmpty) {
                                  //     ScaffoldMessenger.of(context)
                                  //         .showSnackBar(SnackBar(
                                  //             content: Text(
                                  //                 "about can't be empty")));
                                  //     return null;
                                  //   } else
                                  //     return null;
                                  // },
                                  // initialValue: "Hey there, I am on Chattie",
                                  decoration: InputDecoration(
                                    hintText: "Enter About",
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Icon(Icons.phone, size: 30),
                              SizedBox(width: 20),
                              Expanded(
                                child: TextFormField(
                                  // controller: widget.currentUser.phoneNo,
                                  readOnly: true,
                                  initialValue: widget.currentUser.phoneNo,
                                  validator: (value) {
                                    if (value!.isEmpty)
                                      return "about can't be empty";
                                    else
                                      return null;
                                  },
                                  // initialValue: "Hey there, I am on Chattie",
                                  decoration: InputDecoration(
                                    hintText: "Enter About",
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    // Spacer(flex: 3),
                    // ElevatedButton(
                    //   onPressed: () async {
                    //     try {
                    //       await FirebaseAuth.instance.signOut();
                    //     } catch (e) {
                    //       print(e);
                    //     }
                    //     Navigator.of(context).pushAndRemoveUntil(
                    //       CupertinoPageRoute(
                    //         builder: (BuildContext context) => LoginScreen(),
                    //       ),
                    //       (route) => false,
                    //     );
                    //   },
                    //   child: Text("Logout"),
                    // ),
                    // Spacer()
                  ],
                ),
              ),
            ),
            if (isLoading) Center(child: circleLoadingAnimation()),
            // Positioned(
            //   bottom: 20,
            //   left: 10,
            //   right: 10,
            //   child: ElevatedButton(
            //     style: ElevatedButton.styleFrom(
            //       minimumSize: Size(size.width, 56),
            //       textStyle:
            //           TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            //     ),
            //     onPressed: () async {
            //       try {
            //         await FirebaseAuth.instance.signOut();
            //       } catch (e) {
            //         print(e);
            //       }
            //       Navigator.of(context).pushAndRemoveUntil(
            //         CupertinoPageRoute(
            //           builder: (BuildContext context) => LoginScreen(),
            //         ),
            //         (route) => false,
            //       );
            //     },
            //     child: Text("Logout"),
            //   ),
            // )
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(20),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            minimumSize: Size(size.width, 56),
            textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
          onPressed: () async {
            try {
              await FirebaseAuth.instance.signOut();
            } catch (e) {
              print(e);
            }
            Navigator.of(context).pushAndRemoveUntil(
              CupertinoPageRoute(
                builder: (BuildContext context) => LoginScreen(),
              ),
              (route) => false,
            );
          },
          child: Text("Logout",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w700),),
        ),
      ),
    );
  }
}
