import 'package:cached_network_image/cached_network_image.dart';
import 'package:chattie/bloc/chat_bloc.dart';
import 'package:chattie/models/user_model.dart';
import 'package:chattie/screens/chat_screen.dart';
import 'package:chattie/services/constants.dart';
import 'package:chattie/services/database_methods.dart';
import 'package:chattie/services/helper_functions.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';

class AllContactsScreen extends StatefulWidget {
  @override
  _AllContactsScreenState createState() => _AllContactsScreenState();
}

class _AllContactsScreenState extends State<AllContactsScreen> {
  // List<Contact> _contacts = [];
  List<Contact> contactsFilterd = [];
  List<UserModel> _allUsers = [];
  List<UserModel> _allUserFilterd = [];
  UserModel? currentUser;
  TextEditingController _searchController = TextEditingController();
  bool _isSearching = false;
  ScrollController scrollController = ScrollController();
  List<String> nameList = [];

  // void _startSearch() {
  //   // print("open search box");
  //   // ModalRoute.of(context)!
  //   //     .addLocalHistoryEntry(new LocalHistoryEntry(onRemove: _stopSearching));

  //   setState(() {
  //     _isSearching = true;
  //   });
  // }

  // void _stopSearching() {
  //   _clearSearchQuery();

  //   setState(() {
  //     _isSearching = false;
  //   });
  // }

  // void _clearSearchQuery() {
  //   print("close search box");
  //   setState(() {
  //     _searchController.clear();
  //     updateSearchQuery("Search query");
  //   });
  // }

  // void updateSearchQuery(String newQuery) {
  //   setState(() {});
  //   print("search query " + newQuery);
  // }

  Widget _buildSearchField() {
    return TextField(
      controller: _searchController,
      textInputAction: TextInputAction.done,
      autofocus: true,
      cursorColor: Colors.white,
      decoration: const InputDecoration(
        hintText: 'Search...',
        border: InputBorder.none,
        hintStyle: const TextStyle(color: Colors.white30),
      ),
      style: const TextStyle(color: Colors.white, fontSize: 16.0),
      // onChanged: updateSearchQuery,
    );
  }

  Future<List<Contact>> getContacts() async {
    //We already have permissions for contact when we get to this page,
    // so we are now just retrieving it
    PermissionStatus status = await Permission.contacts.status;
    if (status == PermissionStatus.granted) {
      print("permission granted");
      Iterable<Contact> contacts =
          await ContactsService.getContacts(withThumbnails: false);
      List<Contact> cons = contacts.toList();
      // contacts.forEach((element) {
      //   if (element.phones!.length > 0 &&
      //       element.phones!.first.value!.isNotEmpty) {
      // cons.forEach((con) {
      //   if (con.phones!.first.value! != element.phones!.first.value!)
      //     cons.add(element);
      // });
      // cons.add(element);
      //   }
      // });
      // cons = cons.toSet().toList();
      cons.retainWhere((contact) {
        if (contact.phones!.length > 0 &&
            contact.phones!.first.value!.isNotEmpty) {
          return true;
        } else {
          return false;
        }
      });

      // cons.retainWhere((contact) {
      //   print(cons.any((element) =>
      //       element.phones!.first.value == contact.phones!.first.value));
      //   return !cons.any((element) =>
      //       element.phones!.first.value == contact.phones!.first.value);
      // });

      // setState(() {
      //   _contacts = cons;
      // });
      // contacts.where((element) =>
      // element.phones!.length > 0 &&
      // element.phones!.first.value != null
      // &&!contacts.contains(element)
      // );
      // print(cons.length);
      // setState(() {
      //   _contacts = cons;
      // });
      return cons;
    }
    return [];
  }

  onScreenLoad() async {
    // Iterable<Contact> dummy =
    //     await ContactsService.getContactsForPhone('+911234567890');

    // print("finding dummy ${dummy.first.phones!.first.value}");
    List<Contact> allContacts = await getContacts();

    List<String> allPhoneno = [];
    allContacts.forEach((element) {
      allPhoneno.add(flattenPhoneNum(element.phones!.first.value!));
    });

    currentUser = await DatabaseMethods()
        .getUserByUid(FirebaseAuth.instance.currentUser!.uid);

    _allUsers =
        await DatabaseMethods().getAllUser(allPhoneno, currentUser!.name);

    contactsFilterd = allContacts;

    List<String> inviteNumbers = [];

    _allUsers.forEach((element) {
      inviteNumbers.add(flattenPhoneNum(element.phoneNo));
    });

    allPhoneno.removeWhere((element) => inviteNumbers.contains(flattenPhoneNum(element)));

    inviteNumbers = allPhoneno;

    print("invite ${inviteNumbers.length}");

    contactsFilterd = allContacts
        .where((ele) =>
            inviteNumbers.contains(flattenPhoneNum(ele.phones!.first.value!)))
        .toList();

    setState(() {});
    print("all user ${contactsFilterd.length}");
  }

  String flattenPhoneNum(String phone) {
    return phone.replaceAll(RegExp(r'^(\+91)|\D'), '');
    // return phone.replaceAll("+91", "");
    // return phone.replaceAllMapped(RegExp(r'^(\+)|\D'), (Match match) {
    //   return match[0] == "+" ? "+" : "";
    // });
  }

  filterContacts() {
    List<UserModel> users = [];
    users.addAll(_allUsers);
    setState(() {
      _allUserFilterd = users;
    });
    if (_searchController.text.isNotEmpty) {
      users.retainWhere((contact) {
        String searchTerm = _searchController.text.toLowerCase();
        String searchTermFlatten = flattenPhoneNum(searchTerm);
        String contactName = contact.name.toLowerCase();
        bool nameMaches = contactName.contains(searchTerm);
        if (nameMaches) return true;
        if (searchTermFlatten.isEmpty) return false;
        if (contact.phoneNo.isNotEmpty) {
          String phoneFlattened = flattenPhoneNum(contact.phoneNo);
          return phoneFlattened.contains(searchTermFlatten);
        }

        return false;
        //
      });
      setState(() {
        _allUserFilterd = users;
      });
    }
  }

  @override
  void initState() {
    // getContacts();
    onScreenLoad();
    _searchController.addListener(() {
      filterContacts();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // backgroundColor: color1,
          leading: BackButton(
            onPressed: _isSearching
                ? () => setState(() {
                      _isSearching = !_isSearching;
                    })
                : () => Navigator.of(context).pop(),
          ),
          title: _isSearching ? _buildSearchField() : Text("Select Contact"),
          actions: [
            if (_isSearching && _searchController.text.isNotEmpty)
              IconButton(
                onPressed: () {
                  setState(() {
                    _searchController.clear();
                  });
                },
                icon: Icon(Icons.clear),
              ),
            if (!_isSearching)
              IconButton(
                onPressed: () {
                  setState(() {
                    _isSearching = true;
                  });
                },
                icon: Icon(Icons.search),
              )
          ],
        ),
        body: ListView(
          // primary: true,
          shrinkWrap: true,
          children: [
            _allUsers.isEmpty
                ? SizedBox.shrink()
                : Stack(
                    children: [
                      ListView.builder(
                        primary: false,
                        shrinkWrap: true,
                        itemCount: _isSearching
                            ? _allUserFilterd.length
                            : _allUsers.length,
                        itemBuilder: (context, index) {
                          UserModel user = _isSearching
                              ? _allUserFilterd[index]
                              : _allUsers[index];
                          return ListTile(
                            title: Text(
                              user.name,
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w600),
                            ),
                            subtitle: Text(user.about),
                            leading: CachedNetworkImage(
                              placeholder: (context, url) => personIconBuild(),
                              errorWidget:
                                  (BuildContext context, String url, error) =>
                                      personIconBuild(),
                              imageUrl: user.imgUrl,
                              fit: BoxFit.cover,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                width: 45.0,
                                height: 45.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context).pushReplacement(
                                CupertinoPageRoute(
                                  builder: (context) => BlocProvider(
                                    create: (context) =>
                                        ChatBloc(InitialChatState()),
                                    child: ChatScreen(
                                        chatWithUsername: user.name,
                                        myUserName: currentUser!.name),
                                  ),
                                ),
                              );
                            },
                          );
                        },
                      ),
                    ],
                  ),
            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
              child: Text(
                "Invite to Chattie",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
            ListView.builder(
                primary: false,
                shrinkWrap: true,
                itemCount: contactsFilterd.length,
                itemBuilder: (context, index) {
                  Contact user = contactsFilterd[index];
                  return ListTile(
                    title: Text(
                      user.displayName!,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                    subtitle: Text(user.phones!.first.value!),
                    leading: CachedNetworkImage(
                      placeholder: (context, url) => personIconBuild(),
                      errorWidget: (BuildContext context, String url, error) =>
                          personIconBuild(),
                      imageUrl: "",
                      fit: BoxFit.cover,
                      imageBuilder: (context, imageProvider) => Container(
                        width: 45.0,
                        height: 45.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: imageProvider, fit: BoxFit.cover),
                        ),
                      ),
                    ),
                    trailing: Text(
                      "Invite",
                      style: TextStyle(
                          color: color1,
                          fontWeight: FontWeight.w700,
                          fontSize: 14),
                    ),
                    onTap: () {},
                  );
                })
          ],
        ));
  }
}
