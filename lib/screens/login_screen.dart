import 'package:chattie/bloc/chatrooms_bloc.dart';
import 'package:chattie/screens/registeration_details_screen.dart';
import 'package:chattie/services/helper_functions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'home_screen.dart';
import 'otp_verficaction_screen.dart';
import 'package:country_calling_code_picker/picker.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController _phoneController = TextEditingController();
  FirebaseAuth _auth = FirebaseAuth.instance;
  // final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  Country _selectedCountry = Country("", "", "", "");

  void getDefault() async {
    _selectedCountry = (await getCountryByCountryCode(context, "IN"))!;
    setState(() {});
  }

  void onScreenLoad() async {
    if (_auth.currentUser != null) {
      final QuerySnapshot result = await FirebaseFirestore.instance
          .collection('users')
          .where('uid', isEqualTo: _auth.currentUser!.uid)
          .get();
      if (result.docs.length > 0) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => BlocProvider(
              create: (context) => ChatRoomsBloc(InitialChatRoomsState()),
              child: HomeScreen(),
            ),
          ),
          (route) => false,
        );
      } else {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => RegisterDetailsScreen()),
          (route) => false,
        );
      }
    }
  }

  void _onPressedShowBottomSheet() async {
    final country = await showCountryPickerSheet(
      context,
      title: Container(
          padding: EdgeInsets.only(top: 10),
          height: 40,
          child: Text(
            "Choose Region",
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
          )),
      // cancelWidget:
    );
    if (country != null) {
      print("code ${country.countryCode}");
      setState(() {
        _selectedCountry = country;
      });
    }
  }

  @override
  void initState() {
    getDefault();
    // onScreenLoad();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 50),
              Text('Enter your Phone No',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
              SizedBox(height: 10),
              Text(
                  "You will receive a 6 digit code for phone number verification",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                  maxLines: 1),
              SizedBox(height: 30),
              Container(
                // height: 200,
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Country/Region",
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        _selectedCountry.flag.isNotEmpty
                            ? Image.asset(
                                _selectedCountry.flag,
                                package: countryCodePackageName,
                                width: 35,
                                height: 30,
                              )
                            : Container(
                                width: 35,
                                height: 30,
                              ),
                        SizedBox(width: 10),
                        Text(
                          _selectedCountry.name +
                              "(${_selectedCountry.callingCode})",
                          style: TextStyle(
                              color: Colors.green,
                              fontWeight: FontWeight.w600,
                              fontSize: 18),
                        ),
                        Spacer(),
                        IconButton(
                            onPressed: () => _onPressedShowBottomSheet(),
                            icon: Icon(Icons.arrow_drop_down_rounded,
                                color: Colors.black54)),
                      ],
                    ),
                    // SizedBox(height: 5),
                    Divider(color: Colors.white),
                    SizedBox(height: 5),
                    Text("Enter Phone Number",
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    TextField(
                      controller: _phoneController,
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.done,
                      style: TextStyle(color: Colors.green, fontSize: 18),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "phone number",
                      ),
                      // validator: (value) {
                      //   if (value!.isEmpty) {
                      //     return "phone number can not be empty";
                      //   } else if (value.length < 10 || value.length > 10) {
                      //     return "invalid phone number";
                      //   }
                      // },
                      // decoration: InputDecoration.
                      // (
                      // prefix: CountryCodePicker(
                      //   initialSelection: "IN",
                      //   showFlag: true,
                      //   onChanged: (countrycode) {
                      //     print("code : ${countrycode.dialCode}");
                      //     setState(() {
                      //       _countryCode = countrycode.code!;
                      //     });
                      //   },
                      // ),
                      // enabledBorder: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(5)),
                      // errorBorder: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(5)),
                      // focusedErrorBorder: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(5)),
                      // focusedBorder: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(5)),
                      // hintText: "Enter Phone Number",
                      // ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              ElevatedButton(
                // style: ElevatedButton.styleFrom(
                //   primary: color1,
                //   minimumSize: Size(double.infinity, 56),
                //   shape: RoundedRectangleBorder(
                //     borderRadius: BorderRadius.circular(10),
                //   ),
                // ),
                onPressed: () async {
                  if (_phoneController.text.isEmpty ||
                      _phoneController.text.length < 10 ||
                      _phoneController.text.length > 10) {
                    // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    //     content: Text("Invalid Format of phone number")));
                    showToast("invalid phone number format");
                  } else {
                    Navigator.push(
                      context,
                      CupertinoPageRoute(
                          builder: (context) => OtpVerficationScreen(
                                phoneNo: _phoneController.text,
                                countryCode: _selectedCountry.callingCode,
                              )),
                    );
                  }
                },
                child: Text("Continue",
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.white)),
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
}
