import 'package:flutter/material.dart';
import 'package:extended_image/extended_image.dart';

class ZoomPhotoScreen extends StatefulWidget {
  final String url, friendName;

  ZoomPhotoScreen({required this.url, required this.friendName});
  @override
  _ZoomPhotoScreenState createState() => _ZoomPhotoScreenState();
}

class _ZoomPhotoScreenState extends State<ZoomPhotoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(
          widget.friendName,
          // style: Theme.of(context)
          //     .textTheme
          //     .headline3!
          //     .copyWith(color: Colors.white),
        ),
      ),
      body: Hero(
        tag: "tag",
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: ExtendedImage.network(
            widget.url,
            fit: BoxFit.contain,
            mode: ExtendedImageMode.gesture,
            initGestureConfigHandler: (state) {
              return GestureConfig(
                minScale: 0.9,
                animationMinScale: 0.7,
                maxScale: 3.0,
                animationMaxScale: 3.5,
                speed: 1.0,
                inertialSpeed: 100.0,
                initialScale: 1.0,
                inPageView: false,
                initialAlignment: InitialAlignment.center,
              );
            },
          ),
        ),
        // PhotoView(
        //   imageProvider: CachedNetworkImageProvider(
        //     widget.url,
        //   ),
        // ),
      ),
    );
  }
}
