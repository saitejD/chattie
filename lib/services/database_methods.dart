import 'dart:async';

import 'package:chattie/models/last_message_model.dart';
import 'package:chattie/models/message_model.dart';
import 'package:chattie/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';


class DatabaseMethods {
  FirebaseFirestore _firestore = FirebaseFirestore.instance;

  getChatRoomIdByUsernames(String a, String b) {
    if (a.substring(0, 1).codeUnitAt(0) > b.substring(0, 1).codeUnitAt(0)) {
      return "$b\_$a";
    } else {
      return "$a\_$b";
    }
  }

  Future<Stream<List<LastMessage>>> getChatRooms(String myUsername) async {
    StreamController<List<LastMessage>>? _lastMessagesController =
        StreamController<List<LastMessage>>();
    FirebaseFirestore.instance
        .collection("chatrooms")
        .where("users", arrayContains: myUsername)
        .orderBy("lastMessageSendTs", descending: true)
        .snapshots()
        .listen((lastMessageSnapshot) {
      if (lastMessageSnapshot.docs.isNotEmpty) {
        List<LastMessage> lastMessage = lastMessageSnapshot.docs
            .map((snapshot) => LastMessage.fromFirestore(snapshot))
            .toList();
        _lastMessagesController.add(lastMessage);
      }
    });
    // _lastMessagesController.close();
    return _lastMessagesController.stream;
  }

    Future<Stream<List<LastMessage>>> getChatRoomsWithFriends(String myUsername,String friendName) async {
    StreamController<List<LastMessage>>? _lastMessagesController =
        StreamController<List<LastMessage>>();
    FirebaseFirestore.instance
        .collection("chatrooms")
        .where("users", arrayContains: myUsername)
        .where("users",arrayContainsAny: [friendName])
        .orderBy("lastMessageSendTs", descending: true)
        .snapshots()
        .listen((lastMessageSnapshot) {
      if (lastMessageSnapshot.docs.isNotEmpty) {
        List<LastMessage> lastMessage = lastMessageSnapshot.docs
            .map((snapshot) => LastMessage.fromFirestore(snapshot))
            .toList();
        _lastMessagesController.add(lastMessage);
      }
    });
    // _lastMessagesController.close();
    return _lastMessagesController.stream;
  }


  Future<List<LastMessage>> getAllChatRooms(String myUsername) async {
    List<LastMessage> lastMessages = [];
    QuerySnapshot<Map<String, dynamic>> querySnapshot = await FirebaseFirestore
        .instance
        .collection("chatrooms")
        .where("users", arrayContains: myUsername)
        .orderBy("lastMessageSendTs", descending: true)
        .get();

    if (querySnapshot.docs.isNotEmpty) {
      lastMessages = querySnapshot.docs
          .map((snapshot) => LastMessage.fromFirestore(snapshot))
          .toList();
    }
    return lastMessages;
  }

  Future<List<UserModel>> getAllUser(
      List<String> contactNums, String myUserName) async {
    QuerySnapshot<Map<String, dynamic>> all = await _firestore
        .collection("users")
        .where("name", isNotEqualTo: myUserName)
        .orderBy("name", descending: false)
        .get();
    List<UserModel> users = [];
    all.docs.forEach((element) {
      String phNo = element.get("phoneNo").toString();
      String phNorem = phNo.replaceAll("+91", "");
      print("phNorem $phNorem ${phNorem.runtimeType}");
      if (contactNums.contains(phNorem)) {
        print("here");
        users.add(UserModel.fromFirebaseUserModel(element));
      }
    });
    users.toSet().toList();
    return users;
  }

  Future<dynamic> createChatRoom(
      String chatRoomId, Map<String, dynamic> chatRoomInfoMap) async {
    final snapShot = await FirebaseFirestore.instance
        .collection("chatrooms")
        .doc(chatRoomId)
        .get();
    if (snapShot.exists) {
      // chatroom already exists
      return true;
    } else {
      // chatroom does not exists
      return FirebaseFirestore.instance
          .collection("chatrooms")
          .doc(chatRoomId)
          .set(chatRoomInfoMap);
    }
  }

  Future addMessage(
      String chatRoomId, Map<String, dynamic> messageInfoMap) async {
    return FirebaseFirestore.instance
        .collection("chatrooms")
        .doc(chatRoomId)
        .collection("chats")
        .add(messageInfoMap);
  }

  Future updateUserData(String docId, Map<String, dynamic> user) {
    return _firestore.collection("users").doc(docId).update(user);
  }

  updateLastMessageSend(
      String chatRoomId, Map<String, dynamic> lastMessageInfoMap) async {
    return FirebaseFirestore.instance
        .collection("chatrooms")
        .doc(chatRoomId)
        .update(lastMessageInfoMap);
  }

  Future<UserModel> getUserByName(String name) async {
    QuerySnapshot query = await _firestore
        .collection("users")
        .where("name", isEqualTo: name)
        .get();
    return query.docs.map((doc) => UserModel.fromFirebaseUserModel(doc)).first;
  }

  Stream<UserModel> getFriendStream(String name) {
    final StreamController<UserModel> userStreamController =
        StreamController<UserModel>();
    _firestore
        .collection("users")
        .where("name", isEqualTo: name)
        .snapshots()
        .listen((snapshots) {
      var user = snapshots.docs
          .map((user) => UserModel.fromFirebaseUserModel(user))
          .first;
      print("friend stream ${user.status}");
      userStreamController.add(user);
    });
    return userStreamController.stream;
  }

  Future<UserModel> getUserByUid(String uid) async {
    DocumentSnapshot user =
        await FirebaseFirestore.instance.collection("users").doc(uid).get();
    return UserModel.fromFirebaseUserModel(user);
  }

  updateIsRead(String chatRoomId, Map<String, dynamic> lastMessageInfoMap,
      String myUserName) async {
    print("myUserName - $myUserName");
    final snapShot = await FirebaseFirestore.instance
        .collection("chatrooms")
        .doc(chatRoomId)
        .get();
    // make isRead only if lastMessageSendBy not current user
    if (snapShot.exists &&
        snapShot.data()!['lastMessageSendBy'] != myUserName) {
      return FirebaseFirestore.instance
          .collection("chatrooms")
          .doc(chatRoomId)
          .update(lastMessageInfoMap);
    }
  }

  void setStatus(String status) async {
    User? user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      await FirebaseFirestore.instance
          .collection("users")
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .update({"status": status});
    }
  }

  void deleteMessage(String chatRoomId, String msgId) {
    _firestore
        .collection("chatrooms")
        .doc(chatRoomId)
        .collection("chats")
        .doc(msgId)
        .update({"deleted": true});
  }

  Future<List<MessageModel>> getAllMessages(String chatRoomId) async {
    QuerySnapshot<Map<String, dynamic>> messages = await _firestore
        .collection("chatrooms")
        .doc(chatRoomId)
        .collection("chats")
        .get();
    List<MessageModel> allMessages = [];
    allMessages = messages.docs.map((e) => MessageModel.fromJson(e)).toList();
    return allMessages;
  }

  //testing here
  // void sendMessage(
  //     String content,
  //     int type,
  //     List<Message> messages,
  //     String myUserName,
  //     String chatWithUsername,
  //     String replyMessage,
  //     String sendBy,
  //     String chatRoomId) async {
  //   if (messages.length == 0) {
  //     print("no msgs ${messages.length}");
  //     UserModel currentUser = await DatabaseMethods().getUserByName(myUserName);
  //     UserModel friend =
  //         await DatabaseMethods().getUserByName(chatWithUsername);
  //     Map<String, dynamic> chatRoomInfoMap = {
  //       "users": [currentUser.name, friend.name],
  //       "usersPics": {
  //         myUserName: currentUser.imgUrl,
  //         chatWithUsername: friend.imgUrl,
  //       },
  //     };
  //     await DatabaseMethods().createChatRoom(chatRoomId, chatRoomInfoMap);
  //   }
  //   if (content.trim() != '') {
  //     var lastMessageTs = DateTime.now().millisecondsSinceEpoch.toString();
  //     Message message = Message(
  //       content: content,
  //       sendBy: myUserName,
  //       timeStamp: lastMessageTs,
  //       type: type,
  //       repliedMsgId: .
  //       // replyMessage: replyMessage,
  //       // repliedTo: sendBy,
  //       // id: randomAlphaNumeric(12),
  //     );
  //     Map<String, dynamic> lastMessageInfoMap = {
  //       "lastMessage": content,
  //       "lastMessageSendTs": lastMessageTs,
  //       "lastMessageSendBy": myUserName,
  //       'type': type,
  //       'isRead': false,
  //     };
  //     DatabaseMethods().addMessage(chatRoomId, message.toJson());
  //     DatabaseMethods().updateLastMessageSend(chatRoomId, lastMessageInfoMap);
  //   }
  // }
}
