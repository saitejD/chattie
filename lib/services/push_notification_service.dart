import 'package:chattie/services/local_notification_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationService {
  // final BuildContext context;

  // PushNotificationService({required this.context});

  // awesome local notification servie instance
  LocalNotificationService notificationService = LocalNotificationService();

  // function to handle push notification on background
  Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
    // showNotification(message);
    notificationService.showAwesomeNotification(message);
  }

  // function to handle foreground notifications
  void initialFirebaseMessaging() {
    // initLocalNotificaions();
    notificationService.initAwesomeNotification();

    ///gives you the message on which user taps
    ///and it opened the app from terminated state
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {
        String routeFromMessage = message.data["payload"];
        print("get initial messsage block $routeFromMessage");
      }
    });

    ///forground work
    FirebaseMessaging.onMessage.listen((message) {
      if (message.notification != null) {
        print("notification data ${message.notification!.android!.imageUrl}");
      }
      // showNotification(message);
      notificationService.showAwesomeNotification(message);
    });

    ///When the app is in background but opened and user taps on the notification
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      final String routeFromMessage = message.data["payload"];
      print("payload $routeFromMessage");
    });
  }

  Future<String?> getFirebaseToken() async {
    return await FirebaseMessaging.instance.getToken();
  }

  Future<void> sendPushNotification() async{
    await FirebaseMessaging.instance.sendMessage(
      to: "ef7y_ZUhSI2X9b6OL1L0IR:APA91bGesFOphLa_ZdyC0MLcu30onoyBi9w-X7VF1vGUd4lRFulOp46v_08kvnGxysZVFL9tCJ_9e17UjzGkXz3VYcZdarIqBuwKQoExItNlRgHzP1MbR_rwMGMAQyhYIZG71MaiepZo",
      data: {"payload": "123"},
      // messageId: "",
      // messageType: "",
    );
  }
}
