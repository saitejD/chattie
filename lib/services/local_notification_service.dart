import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class LocalNotificationService {
  // for factory constrouctr returning
  static final LocalNotificationService _notificationService =
      LocalNotificationService._internal();

  LocalNotificationService._internal();

  factory LocalNotificationService() {
    return _notificationService;
  }

  /// awesome notifications setup

  void initAwesomeNotification() {
    AwesomeNotifications().initialize('resource://mipmap/ic_launcher', [
      // notification icon
      NotificationChannel(
        channelGroupKey: 'basic_test',
        channelKey: 'basic',
        channelName: 'Basic notifications',
        channelDescription: 'Notification channel for basic tests',
        channelShowBadge: true,
        importance: NotificationImportance.High,
        enableVibration: true,
      ),

      NotificationChannel(
          channelGroupKey: 'image_test',
          channelKey: 'image',
          channelName: 'image notifications',
          channelDescription: 'Notification channel for image tests',
          // defaultColor: Colors.redAccent,
          ledColor: Colors.white,
          channelShowBadge: true,
          importance: NotificationImportance.High)

      //add more notification type with different configuration
    ]);
  }

  void showAwesomeNotification(RemoteMessage message) async {
    if (message.notification != null) {
      AwesomeNotifications().createNotification(
        content: NotificationContent(
          //with asset image
          id: 1234,
          channelKey: 'image',
          title: message.notification!.title,
          body: message.notification!.body,
          // bigPicture: message.notification!.android!.imageUrl,
          icon: "",
          notificationLayout: NotificationLayout.Default,
          payload: Map<String, String>.from(message.data),
        ),
      );
    }
  }
}
