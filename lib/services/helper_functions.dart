import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

String getStatus(String status) {
  if (status == "Online") {
    return "Online";
  } else {
    return timeago.format(
      DateTime.fromMillisecondsSinceEpoch(int.parse(status)),
    );
  }
}

String getDate(String d) {
  String date = '';
  if (int.parse(DateFormat('dd')
          .format(DateTime.fromMillisecondsSinceEpoch(int.parse(d)))) ==
      (int.parse(DateFormat("dd").format(DateTime.now())) - 1)) {
    date = 'Yesterday';
  } else if (DateFormat('dd')
          .format(DateTime.fromMillisecondsSinceEpoch(int.parse(d))) ==
      (DateFormat("dd").format(DateTime.now()))) {
    date = "Today";
  } else {
    date = DateFormat('dd MMM y')
        .format(DateTime.fromMillisecondsSinceEpoch(int.parse(d)));
  }
  return date;
}

Widget personIconBuild([double w = 40, double h = 40]) {
  return Container(
    width: w,
    height: h,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      color: Colors.grey,
    ),
    child: Icon(
      Icons.person,
      color: Colors.white,
    ),
  );
}

Widget profilePhoto(String imgUrl,[double w = 40, double h = 40]) {
  return CachedNetworkImage(
    placeholder: (context, url) => personIconBuild(),
    errorWidget: (BuildContext context, String url, error) => personIconBuild(),
    imageUrl: imgUrl,
    imageBuilder: (context, imageProvider) => Container(
      width: w,
      height: h,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          image: imageProvider,
          fit: BoxFit.contain,
        ),
      ),
    ),
  );
}

showToast(String message) {
  return Fluttertoast.showToast(
    msg: message,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
    backgroundColor: Colors.black,
    textColor: Colors.white,
    fontSize: 16.0,
  );
}

threeBounceLoadingAnimation() {
  return SpinKitThreeBounce(color: Colors.green, size: 25);
}

circleLoadingAnimation() {
  return SpinKitCircle(
    color: Colors.green,
  );
}


  Widget messageLoading() {
    return ListView.builder(
      itemCount: 4,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return ListTile(
          onTap: () {},
          leading: CircleAvatar(
            backgroundColor: Colors.grey[350]!.withOpacity(0.6),
            radius: 25,
          ),
          title: SizedBox(
            height: 10,
            width: 100,
            child: Container(color: Colors.black.withOpacity(0.2)),
          ),
          subtitle: SizedBox(
            height: 10,
            width: 60,
            child: Container(color: Colors.black.withOpacity(0.2)),
          ),
        );
      },
    );
  }