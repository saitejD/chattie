import 'package:chattie/services/helper_functions.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

Future<bool> askPermissions() async {
  PermissionStatus permissionStatus = await _getContactPermission();
  if (permissionStatus == PermissionStatus.granted) {
    return true;
  } else {
    return await _handleInvalidPermissions(permissionStatus);
  }
}

Future<bool> _handleInvalidPermissions(
    PermissionStatus permissionStatus) async {
  if (permissionStatus == PermissionStatus.denied) {
    showToast('Need to grant permission to proceed further');
    return await askPermissions();
  } else if (permissionStatus == PermissionStatus.permanentlyDenied) {
    showToast(
        'You permanently denined permission you can not proceed further!');
    return false;
  } else {
    return await askPermissions();
  }
}

Future<PermissionStatus> _getContactPermission() async {
  PermissionStatus permissionStatus = await Permission.contacts.status;
  if (permissionStatus != PermissionStatus.granted) {
    // Map<PermissionGroup, PermissionStatus> permissionStatus =
    //     await PermissionHandler()
    //         .requestPermissions([PermissionGroup.contacts]);
    // return permissionStatus[PermissionGroup.contacts] ??
    //     PermissionStatus.unknown;
    PermissionStatus permissionStatus = await Permission.contacts.request();
    return permissionStatus;
  } else {
    return permissionStatus;
  }
}

class Permissions {
  static Future<bool> cameraAndMicrophonePermissionsGranted() async {
    PermissionStatus cameraPermissionStatus = await _getCameraPermission();
    PermissionStatus microphonePermissionStatus =
        await _getMicrophonePermission();

    if (cameraPermissionStatus == PermissionStatus.granted &&
        microphonePermissionStatus == PermissionStatus.granted) {
      return true;
    } else {
      _handleInvalidPermissions(
          cameraPermissionStatus, microphonePermissionStatus);
      return false;
    }
  }

  static Future<PermissionStatus> _getCameraPermission() async {
    PermissionStatus permission = await Permission.camera.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.limited) {
      // You can request multiple permissions at once.
      // Map<Permission, PermissionStatus> statuses = await [
      //   Permission.camera,
      //   Permission.microphone,
      // ].request();
      PermissionStatus status = await Permission.camera.request();
      return status;
      // return permissionStatus[PermissionGroup.camera] ??
      //     PermissionStatus.unknown;
    } else {
      return permission;
    }
  }

  static Future<PermissionStatus> _getMicrophonePermission() async {
    PermissionStatus permission = await Permission.microphone.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.limited) {
      PermissionStatus status = await Permission.microphone.request();
      return status;
      // Map<PermissionGroup, PermissionStatus> permissionStatus =
      //     await PermissionHandler()
      //         .requestPermissions([PermissionGroup.microphone]);
      // return permissionStatus[PermissionGroup.microphone] ??
      //     PermissionStatus.unknown;
    } else {
      return permission;
    }
  }

  static void _handleInvalidPermissions(
    PermissionStatus cameraPermissionStatus,
    PermissionStatus microphonePermissionStatus,
  ) {
    if (cameraPermissionStatus == PermissionStatus.denied &&
        microphonePermissionStatus == PermissionStatus.denied) {
      throw new PlatformException(
          code: "PERMISSION_DENIED",
          message: "Access to camera and microphone denied",
          details: null);
    } else if (cameraPermissionStatus == PermissionStatus.permanentlyDenied &&
        microphonePermissionStatus == PermissionStatus.permanentlyDenied) {
      throw new PlatformException(
          code: "PERMISSION_DISABLED",
          message: "Location data is not available on device",
          details: null);
    }
  }
}
