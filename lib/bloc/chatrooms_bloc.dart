import 'dart:async';
import 'package:chattie/services/database_methods.dart';
import 'package:chattie/models/last_message_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

@immutable
abstract class ChatRoomsEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class StreamChatRoomsEvent extends ChatRoomsEvent {
  final String currentUserName;

  StreamChatRoomsEvent({required this.currentUserName});
  @override
  List<Object> get props => [currentUserName];
}

class StreamSearchEvent extends ChatRoomsEvent {
  final String currentUserName;
  final String friendName;

  StreamSearchEvent({required this.currentUserName,required this.friendName});
  @override
  List<Object> get props => [currentUserName,friendName];
}

class FutureChatRoomsEvent extends ChatRoomsEvent {
  final String currentUserName;

  FutureChatRoomsEvent({required this.currentUserName});
  @override
  List<Object> get props => [];
}

@immutable
abstract class ChatRoomsState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialChatRoomsState extends ChatRoomsState {
  @override
  List<Object> get props => [];
}

class FetchingChatRoomsState extends ChatRoomsState {
  @override
  List<Object> get props => [];
}

class FetchStreamChatRoomsState extends ChatRoomsState {
  final Stream<List<LastMessage>> chatRoomsStream;

  FetchStreamChatRoomsState(this.chatRoomsStream);
  @override
  List<Object> get props => [chatRoomsStream];
}

class FetchFutureChatRoomsState extends ChatRoomsState {
  final List<LastMessage> chatRooms;

  FetchFutureChatRoomsState({required this.chatRooms});

  @override
  List<Object> get props => [chatRooms];
}

class ErrorState extends ChatRoomsState {
  final Exception exception;

  ErrorState(this.exception);

  @override
  String toString() => 'ErrorState';

  @override
  List<Object> get props => [exception];
}

class ChatRoomsBloc extends Bloc<ChatRoomsEvent, ChatRoomsState> {
  Stream<List<LastMessage>>? _lastMessagesController;
  ChatRoomsBloc(ChatRoomsState initialState) : super(InitialChatRoomsState());
  @override
  Stream<ChatRoomsState> mapEventToState(ChatRoomsEvent event) async* {
    if (event is StreamChatRoomsEvent) {
      yield* mapToStreamFetch(event);
    }else if(event is StreamSearchEvent){
      yield* mapToStreamSearch(event);
    } 
    else if (event is FutureChatRoomsEvent) {
      yield* mapToFutureFetch(event);
    }
  }

  Stream<ChatRoomsState> mapToStreamFetch(StreamChatRoomsEvent event) async* {
    yield FetchingChatRoomsState();

    _lastMessagesController =
        await DatabaseMethods().getChatRooms(event.currentUserName);
    yield FetchStreamChatRoomsState(_lastMessagesController!);
  }

    Stream<ChatRoomsState> mapToStreamSearch(StreamSearchEvent event) async* {
    yield FetchingChatRoomsState();

    _lastMessagesController =
        await DatabaseMethods().getChatRoomsWithFriends(event.currentUserName,event.friendName);
    yield FetchStreamChatRoomsState(_lastMessagesController!);
  }

  Stream<ChatRoomsState> mapToFutureFetch(FutureChatRoomsEvent event) async* {
    yield FetchingChatRoomsState();
    List<LastMessage> msgs =
        await DatabaseMethods().getAllChatRooms(event.currentUserName);
    yield FetchFutureChatRoomsState(chatRooms: msgs);
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
