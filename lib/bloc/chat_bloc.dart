import 'dart:async';
import 'dart:io';
import 'package:chattie/models/message_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

@immutable
abstract class ChatEvent extends Equatable {
  @override
  List<Object> get props => [];
}

//triggered to fetch messages of chat, this will also keep a subscription for new messages
class FetchMessagesEvent extends ChatEvent {
  final String currentUserName;
  final String chattingWithUserName;

  FetchMessagesEvent(
      {required this.currentUserName, required this.chattingWithUserName});

  @override
  List<Object> get props => [currentUserName, chattingWithUserName];
  @override
  String toString() => 'FetchMessagesEvent';
}

//triggered to fetch messages of chat
class FetchPreviousMessagesEvent extends ChatEvent {
  final MessageModel lastMessage;
  final String currentUserName;
  final String chattingWithUserName;
  FetchPreviousMessagesEvent(
      this.currentUserName, this.chattingWithUserName, this.lastMessage);

  @override
  String toString() => 'FetchPreviousMessagesEvent';
}

//triggered when messages stream has new data
class ReceivedMessagesEvent extends ChatEvent {
  final List<MessageModel> messages;
  final String username;
  ReceivedMessagesEvent(this.messages, this.username);

  @override
  String toString() => 'ReceivedMessagesEvent';
}

// triggered to send new text message
class SendTextMessageEvent extends ChatEvent {
  final MessageModel message;

  SendTextMessageEvent(this.message);

  @override
  String toString() => 'SendTextMessageEvent {message: $message}';
}

//triggered to send attachment
class SendAttachmentEvent extends ChatEvent {
  final String chatId;
  final File file;
  // final FileType fileType;

  SendAttachmentEvent({required this.chatId, required this.file});

  @override
  String toString() => 'SendAttachmentEvent';
}

@immutable
abstract class ChatState extends Equatable {
  ChatState([List props = const <dynamic>[]]);
}

class InitialChatState extends ChatState {
  @override
  List<Object> get props => [];
}

class FetchingMessageState extends ChatState {
  @override
  String toString() => 'FetchingMessageState';

  @override
  List<Object> get props => [];
}

class FetchedMessagesState extends ChatState {
  final List<MessageModel> messages;
  final String username;
  final bool isPrevious;
  FetchedMessagesState(this.messages, this.username, {required this.isPrevious})
      : super([messages, username, isPrevious]);

  @override
  List<Object> get props => [messages, username, isPrevious];
  @override
  String toString() =>
      'FetchedMessagesState {messages: ${messages.length}, username: $username, isPrevious: $isPrevious}';
}

class ErrorState extends ChatState {
  final String exception;

  ErrorState(this.exception);

  @override
  String toString() => 'ErrorState';

  @override
  List<Object> get props => [exception];
}

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  Map<String, StreamSubscription> messagesSubscriptionMap =
      Map<String, StreamSubscription<dynamic>>();

  ChatBloc(ChatState initialState) : super(InitialChatState());

  Stream<ChatState> mapEventToState(ChatEvent event) async* {
    print("Event $event");
    if (event is FetchMessagesEvent) {
      yield* mapFetchMessagesEventToState(event);
    } else if (event is FetchPreviousMessagesEvent) {
      yield* mapFetchPreviousMessagesEventToState(event);
    } else if (event is ReceivedMessagesEvent) {
      yield FetchedMessagesState(event.messages, event.username,
          isPrevious: false);
    }
    //  else if (event is SendTextMessageEvent) {
    //   yield* mapToSendMessage(event);
    // }
  }

  // mapToSendMessage(SendTextMessageEvent event) async* {
  //   DatabaseMethods().sendMessage(
  //     messageTextEdittingController.text,
  //     0,
  //     messages,
  //     myUserName,
  //     chatWithUsername,
  //     replyMessage,
  //     sendBy,
  //     chatRoomId,
  //   );
  // }

  Stream<ChatState> mapFetchMessagesEventToState(
      FetchMessagesEvent event) async* {
    try {
      yield FetchingMessageState();
      String chatRoomId = getChatRoomIdByUsernames(
          event.chattingWithUserName, event.currentUserName);

      // StreamSubscription<List<Message>> messagesSubscription =
      //     messagesSubscriptionMap[chatRoomId];
      // messagesSubscription.cancel();

      StreamSubscription<List<MessageModel>> messagesSubscription =
          getMessages(chatRoomId).listen((messages) {
        add(ReceivedMessagesEvent(messages, event.currentUserName));
      });
      messagesSubscriptionMap[chatRoomId] = messagesSubscription;
      // messagesSubscription.cancel();
    } catch (exception) {
      print("exception $exception");
      yield ErrorState(exception.toString());
    }
  }

  Stream<ChatState> mapFetchPreviousMessagesEventToState(
      FetchPreviousMessagesEvent event) async* {
    try {
      yield FetchingMessageState();
      String chatRoomId = getChatRoomIdByUsernames(
          event.chattingWithUserName, event.currentUserName);
      final messages = await getPreviousMessages(chatRoomId, event.lastMessage);
      yield FetchedMessagesState(messages, event.currentUserName,
          isPrevious: true);
    } catch (exception) {
      // print(exception.errorMessage());
      yield ErrorState(exception.toString());
    }
  }

  @override
  Future<void> close() {
    messagesSubscriptionMap.forEach((_, subscription) => subscription.cancel());
    return super.close();
  }
}

Stream<List<MessageModel>> getMessages(String chatRoomId) {
  // List<Message> messages = List();
  print("till here ");
  Stream<List<MessageModel>> snapshots = FirebaseFirestore.instance
      .collection("chatrooms")
      .doc(chatRoomId)
      .collection("chats")
      .orderBy("timeStamp", descending: true)
      .limit(20)
      .snapshots()
      .transform(StreamTransformer<QuerySnapshot<Map<String, dynamic>>,
              List<MessageModel>>.fromHandlers(
          handleData:
              (QuerySnapshot querySnapshot, EventSink<List<MessageModel>> sink) =>
                  mapDocumentToMessage(querySnapshot, sink)));
  // print(snapshots.first.then((value) => print(value.first.content)));
  return snapshots;
}

Future<List<MessageModel>> getPreviousMessages(
    String chatRoomId, MessageModel prevMessage) async {
  DocumentSnapshot prevDocument;
  prevDocument = await FirebaseFirestore.instance
      .collection("chatrooms")
      .doc(chatRoomId)
      .collection("chats")
      .doc(prevMessage.id)
      .get(); // gets a reference to the last message in the existing list
  print("previous ${prevDocument["content"]}");
  final querySnapshot = await FirebaseFirestore.instance
      .collection("chatrooms")
      .doc(chatRoomId)
      .collection("chats")
      .orderBy('timeStamp', descending: true)
      .startAfterDocument(prevDocument)
      .limit(20)
      .get();
  print("snapshtos ${querySnapshot.docs.length}");
  List<MessageModel> messageList = [];
  // if (querySnapshot.docs.length > 0) {
  print("length");
  querySnapshot.docs.forEach((doc) => messageList.add(MessageModel.fromJson(doc)));
  return messageList;
  // } else
  //   return messageList;
}

void mapDocumentToMessage(QuerySnapshot querySnapshot, EventSink sink) async {
  List<MessageModel> messages = [];
  print("till here 1");
  for (DocumentSnapshot document in querySnapshot.docs) {
    messages.add(MessageModel.fromJson(document));
    print("first msg: ${messages.first.content}");
  }

  sink.add(messages);
}

String getChatRoomIdByUsernames(String a, String b) {
  if (a.substring(0, 1).codeUnitAt(0) > b.substring(0, 1).codeUnitAt(0)) {
    return "$b\_$a";
  } else {
    return "$a\_$b";
  }
}
