import 'package:chattie/services/database_methods.dart';
import 'package:chattie/models/user_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';

class UserProvider with ChangeNotifier {
   UserModel? _user;

  UserModel? get getUser => _user;

  void refreshUser() async {
    UserModel user = await DatabaseMethods()
        .getUserByUid(FirebaseAuth.instance.currentUser!.uid);
    _user = user;
    notifyListeners();
  }
}
