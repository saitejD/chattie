import 'package:cloud_firestore/cloud_firestore.dart';

class Call {
  late String callerId;
  late String callerName;
  late String callerPic;
  late String receiverId;
  late String receiverName;
  late String receiverPic;
  late String channelId;
  late bool hasDialled;
  late String token;
  late bool isVideoCall;

  Call({
    required this.callerId,
    required this.callerName,
    required this.callerPic,
    required this.receiverId,
    required this.receiverName,
    required this.receiverPic,
    required this.channelId,
    required this.hasDialled,
    required this.token,
    required this.isVideoCall,
  });

  // to map
  Map<String, dynamic> toMap(Call call) {
    Map<String, dynamic> callMap = Map();
    callMap["caller_id"] = call.callerId;
    callMap["caller_name"] = call.callerName;
    callMap["caller_pic"] = call.callerPic;
    callMap["receiver_id"] = call.receiverId;
    callMap["receiver_name"] = call.receiverName;
    callMap["receiver_pic"] = call.receiverPic;
    callMap["channel_id"] = call.channelId;
    callMap["has_dialled"] = call.hasDialled;
    callMap["token"] = call.token;
    callMap["isVideoCall"] = call.isVideoCall;
    return callMap;
  }

  Call.fromMap(DocumentSnapshot callMap) {
    this.callerId = callMap["caller_id"];
    this.callerName = callMap["caller_name"];
    this.callerPic = callMap["caller_pic"];
    this.receiverId = callMap["receiver_id"];
    this.receiverName = callMap["receiver_name"];
    this.receiverPic = callMap["receiver_pic"];
    this.channelId = callMap["channel_id"];
    this.hasDialled = callMap["has_dialled"];
    this.token = callMap["token"];
    this.isVideoCall = callMap["isVideoCall"];
  }
}
