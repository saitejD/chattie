import 'dart:convert';
// To parse this JSON data, do
//
//     final invester = investerFromJson(jsonString);
import 'package:cloud_firestore/cloud_firestore.dart';

List<LastMessage> lastMessagesFromFirebase(String str) =>
    List<LastMessage>.from(
        json.decode(str).map((x) => LastMessage.fromFirestore(x)));

String lastMessagesToFirebase(List<LastMessage> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toFirestore())));

class LastMessage {
  LastMessage(
      {required this.content,
      required this.isRead,
      required this.timeStamp,
      required this.type,
      required this.id,
      required this.sendBy,
      required this.users,
      required this.userPics});

  bool isRead;
  String content;
  String timeStamp;
  int type;
  String id;
  String sendBy;
  List<dynamic> users;
  Map<String, dynamic> userPics;

  factory LastMessage.fromFirestore(DocumentSnapshot doc) => LastMessage(
        content: doc["lastMessage"],
        sendBy: doc['lastMessageSendBy'],
        timeStamp: doc['lastMessageSendTs'],
        type: doc['type'],
        id: doc.id,
        users: doc['users'],
        userPics: doc['usersPics'],
        isRead: doc['isRead'],
      );

  Map<String, dynamic> toFirestore() => {
        "lastMessage": content,
        "lastMessageSendBy": sendBy,
        "lastMessageSendTs": timeStamp,
        "type": type,
        "isRead": isRead,
        "users": users,
        "userPics": userPics,
      };
}
