import 'dart:convert';
// To parse this JSON data, do
//
//     final invester = investerFromJson(jsonString);
import 'package:cloud_firestore/cloud_firestore.dart';

List<MessageModel> usersFromJson(String str) =>
    List<MessageModel>.from(json.decode(str).map((x) => MessageModel.fromJson(x)));

String investerToJson(List<MessageModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MessageModel {
  MessageModel({
    required this.content,
    required this.sendBy,
    required this.timeStamp,
    required this.type,
    this.repliedMsgId,
    this.id,
    required this.deleted,
  });

  String content;
  String sendBy;
  String timeStamp;
  int type;
  String? id;
  String? repliedMsgId;
  bool deleted;
  // String? replyMessage;
  // String? repliedTo;

  factory MessageModel.fromJson(DocumentSnapshot json) => MessageModel(
        content: json["content"],
        sendBy: json['sendBy'],
        timeStamp: json['timeStamp'],
        type: json['type'],
        repliedMsgId: json['repliedMsgId'],
        id: json.id,
        deleted: json['deleted'],
      );

  Map<String, dynamic> toJson() => {
        "content": content,
        "sendBy": sendBy,
        "timeStamp": timeStamp,
        "type": type,
        "repliedMsgId": repliedMsgId,
        "deleted": deleted,
      };
}
