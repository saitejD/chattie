import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';

List<UserModel> userModelsFromFirebase(String str) => List<UserModel>.from(
    json.decode(str).map((x) => UserModel.fromFirebaseUserModel(x)));

String userModelsToFirebase(List<UserModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toFirebaseUserModel())));

class UserModel {
  UserModel({
    required this.about,
    required this.createdAt,
    required this.imgUrl,
    required this.name,
    required this.phoneNo,
    required this.status,
    required this.uid,
  });

  String about;
  String createdAt;
  String imgUrl;
  String name;
  String phoneNo;
  String status;
  String uid;

  factory UserModel.fromFirebaseUserModel(DocumentSnapshot doc) => UserModel(
        about: doc["about"],
        createdAt: doc['createdAt'],
        imgUrl: doc['imgUrl'],
        name: doc['name'],
        phoneNo: doc['phoneNo'],
        status: doc['status'],
        uid: doc['uid'],
      );

  Map<String, dynamic> toFirebaseUserModel() => {
        "about": about,
        "createdAt": createdAt,
        "imgUrl": imgUrl,
        "name": name,
        "phoneNo": phoneNo,
        "status": status,
        "uid": uid
      };
}
