import 'dart:async';
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:chattie/bloc/user_provider.dart';
import 'package:chattie/models/call.dart';
import 'package:chattie/services/call_methods.dart';
import 'package:chattie/services/constants.dart';
import 'package:chattie/services/helper_functions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class CallScreen extends StatefulWidget {
  final Call call;

  CallScreen({required this.call});
  @override
  _CallScreenState createState() => _CallScreenState();
}

class _CallScreenState extends State<CallScreen> {
  final CallMethods callMethods = CallMethods();
  late UserProvider userProvider;
  late StreamSubscription callStreamSubscription;
  static final _users = <int>[];
  final _infoStrings = <String>[];
  bool _isMuted = false;
  // static const String APP_ID = "d23d0c1706154d49a05092d31a122f5c";
  static const String APP_ID = "54db6ac2ec9845b0a0c88fbd8091e0bb";

  late RtcEngine _engine;
  int? _remoteUid;
  bool _localUserJoined = false;

  @override
  void initState() {
    super.initState();
    addPostFrameCallback();
    initAgora();
    // initializeAgora();
  }

  @override
  void dispose() {
    // clear users
    _users.clear();
    // destroy sdk
    _engine.leaveChannel();
    // _engine.destroy();
    // AgoraRtcEngine.leaveChannel();
    // AgoraRtcEngine.destroy();
    callStreamSubscription.cancel();
    super.dispose();
  }

  addPostFrameCallback() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      userProvider = Provider.of<UserProvider>(context, listen: false);

      callStreamSubscription = callMethods
          .callStream(uid: userProvider.getUser!.uid)
          .listen((DocumentSnapshot ds) {
        // defining the logic
        switch (ds.exists) {
          case false:
            // snapshot is null which means that call is hanged and documents are deleted
            Navigator.pop(context);
            break;

          default:
            break;
        }
      });
    });
  }

  Future<void> initAgora() async {
    // retrieve permissions
    await [Permission.microphone, Permission.camera].request();

    //create the engine
    _engine = createAgoraRtcEngine();

    await _engine.initialize(RtcEngineContext(
        appId: APP_ID,
        channelProfile: ChannelProfileType.channelProfileLiveBroadcasting,
        logConfig: LogConfig(
            filePath: "", fileSizeInKB: 500, level: LogLevel.logLevelError)));

    _engine.registerEventHandler(
      RtcEngineEventHandler(
        onJoinChannelSuccess: (RtcConnection connection, int elapsed) {
          print("local user ${connection.localUid} joined");
          setState(() {
            _localUserJoined = true;
          });
        },
        onUserJoined: (RtcConnection connection, int remoteUid, int elapsed) {
          print("remote user $remoteUid joined");
          setState(() {
            _remoteUid = remoteUid;
          });
        },
        onUserOffline: (RtcConnection connection, int remoteUid,
            UserOfflineReasonType reason) {
          print("remote user $remoteUid left channel");
          setState(() {
            _remoteUid = null;
          });
        },
        onTokenPrivilegeWillExpire: (RtcConnection connection, String token) {
          debugPrint(
              '[onTokenPrivilegeWillExpire] connection: ${connection.toJson()}, token: $token');
        },
      ),
    );
    await _engine.setLogLevel(LogLevel.logLevelNone);
    print("in init agora $_remoteUid");

    await _engine.setClientRole(
        role: widget.call.hasDialled
            ? ClientRoleType.clientRoleBroadcaster
            : ClientRoleType.clientRoleAudience);
    if (widget.call.isVideoCall) {
      await _engine.enableVideo();
      await _engine.startPreview();
    }
    print(
        "token at join channel ${widget.call.token} and ${widget.call.channelId}");
    await _engine.joinChannel(
      token: widget.call.token,
      channelId: widget.call.channelId,
      uid: 0,
      options: const ChannelMediaOptions(
        clientRoleType: ClientRoleType.clientRoleBroadcaster,
        channelProfile: ChannelProfileType.channelProfileCommunication,
      ),
    );
  }

  // Display remote user's video
  Widget _remoteVideo() {
    if (_remoteUid != null) {
      return AgoraVideoView(
        controller: VideoViewController.remote(
          rtcEngine: _engine,
          canvas: VideoCanvas(uid: _remoteUid),
          connection: RtcConnection(channelId: widget.call.channelId),
        ),
      );
    } else {
      return const Text(
        'Please wait for remote user to join',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
      );
    }
  }

  /// Toolbar layout
  Widget _toolbar() {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.symmetric(vertical: 48),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RawMaterialButton(
            onPressed: () {
              setState(() {
                _isMuted = !_isMuted;
              });
              _engine.muteLocalAudioStream(_isMuted);
            },
            child: Icon(
              _isMuted ? Icons.mic_off : Icons.mic,
              color: _isMuted ? Colors.white : Colors.blueAccent,
              size: 20.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: _isMuted ? Colors.blueAccent : Colors.white,
            padding: const EdgeInsets.all(12.0),
          ),
          RawMaterialButton(
            onPressed: () async {
              await CallMethods().endCall(call: widget.call);
            },
            child: Icon(
              Icons.call_end,
              color: Colors.white,
              size: 35.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.redAccent,
            padding: const EdgeInsets.all(15.0),
          ),
          RawMaterialButton(
            onPressed: widget.call.isVideoCall
                ? () {
                    _engine.switchCamera();
                  }
                : () {},
            child: Icon(
              Icons.switch_camera,
              color: Colors.blueAccent,
              size: 20.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.white,
            padding: const EdgeInsets.all(12.0),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: widget.call.isVideoCall
              ? [
                  Center(
                    child: _remoteVideo(),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: EdgeInsets.all(10),
                      width: 150,
                      height: 180,
                      decoration: BoxDecoration(
                        border: Border.all(color: color1, width: 3),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Center(
                        child: _localUserJoined
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: AgoraVideoView(
                                  controller: VideoViewController(
                                    rtcEngine: _engine,
                                    canvas: const VideoCanvas(uid: 0),
                                  ),
                                ),
                              )
                            : const CircularProgressIndicator(),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: _toolbar(),
                  ),
                ]
              : [
                  Center(
                    child: CachedNetworkImage(
                      placeholder: (context, url) => personIconBuild(),
                      errorWidget: (BuildContext context, String url, error) =>
                          personIconBuild(),
                      imageUrl: widget.call.receiverPic,
                      imageBuilder: (context, imageProvider) => Container(
                        width: size.width,
                        height: size.height * 0.6,
                        decoration: BoxDecoration(
                          // shape: BoxShape.circle,
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: _toolbar(),
                  ),
                ],
        ),
      ),
    );
  }
}
