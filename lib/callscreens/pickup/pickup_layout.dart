import 'package:chattie/bloc/user_provider.dart';
import 'package:chattie/callscreens/pickup/pickup_screen.dart';
import 'package:chattie/models/call.dart';
import 'package:chattie/services/call_methods.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PickupLayout extends StatelessWidget {
  final Widget scaffold;
  final CallMethods callMethods = CallMethods();

  PickupLayout({
    required this.scaffold,
  });

  @override
  Widget build(BuildContext context) {
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    // print("provider ${userProvider.getUser!.uid}");

    return (userProvider.getUser != null)
        ? StreamBuilder<DocumentSnapshot>(
            stream: callMethods.callStream(uid: userProvider.getUser!.uid),
            builder: (context, snapshot) {
              // print("data ${snapshot.data!.id}");
              if (snapshot.hasData && snapshot.data!.exists) {
                // print("call ${snapshot.data!.data}");
                Call call = Call.fromMap(snapshot.data!);
                if (!call.hasDialled) {
                  return PickupScreen(call: call);
                }
              }
              return scaffold;
            },
          )
        : Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }
}
